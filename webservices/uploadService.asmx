<%@ WebService Language="VB" Class="MapService" %>


Imports System
Imports System.IO
Imports System.Security
Imports System.Configuration
Imports System.Xml
Imports System.Web
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Xml.Serialization
Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.Odbc
Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.Collections.Generic
Imports Newtonsoft.Json
Imports System.Net.Http

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
 <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class MapService
    Inherits System.Web.Services.WebService
 
    <WebMethod()> _
    Public Function GetAllActiveSites() As String
		Session.Remove("accessLevel")
		Server.Transfer("/login.aspx", true)
        Return ""

    End Function
    
    
    


    <WebMethod>
    <ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Json)>
    Public Sub GetFile(fileName As String)


        
        Dim actualFile = "//192.168.1.124/ScannedCharts/Signed Consent Forms/" & fileName

        If File.Exists(actualFile) Then

            context.Response.ContentType = "application/pdf"
            context.Response.AddHeader("Content-Disposition", "attachment; filename=""" & Path.GetFileName(actualFile) & """")
            context.Response.TransmitFile(actualFile)

        Else

            context.Response.Clear()
            context.Response.TrySkipIisCustomErrors = True
            context.Response.StatusCode = 404
            context.Response.Write("<html><head><title>404 - File not found</title><style>body {font-family: sans-serif;}</style></head><body><h1>404 - File not found - " & actualFile & "</h1><p>Sorry, that file is not available .</p></body></html>")
            context.Response.End()

        End If


    End Sub




    <WebMethod>
    <ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Json)>
    Public Sub GetEduFile(fileName As String)

    	'\\192.168.1.124\Urgent Care\Patient Education'
        
        Dim actualFile = "//192.168.1.124/Urgent Care/Patient Education/" & fileName

        If File.Exists(actualFile) Then

            context.Response.ContentType = "application/pdf"
            context.Response.AddHeader("Content-Disposition", "attachment; filename=""" & Path.GetFileName(actualFile) & """")
            context.Response.TransmitFile(actualFile)

        Else

            context.Response.Clear()
            context.Response.TrySkipIisCustomErrors = True
            context.Response.StatusCode = 404
            context.Response.Write("<html><head><title>404 - File not found</title><style>body {font-family: sans-serif;}</style></head><body><h1>404 - File not found - " & actualFile & "</h1><p>Sorry, that file is not available .</p></body></html>")
            context.Response.End()

        End If


    End Sub




<WebMethod> _
Public Sub save()

		
	Dim loop1 As Integer
	Dim arr1() As String
	Dim Files As HttpFileCollection
	Dim memFile As HttpPostedFile
	Dim bytReq(0) As Byte


	Dim intLen As Integer


	Dim strParam As String


	Files = HttpContext.Current.Request.Files 
  Dim id As String = HttpContext.Current.Request.Form("id")

	arr1 = Files.AllKeys 


	Dim strResult As String


	Dim strFname As String


	Dim strParams() As String


	For loop1 = 0 To arr1.GetUpperBound(0)


	strResult &= "File control: " & Server.HtmlEncode(arr1(loop1)) & vbCrLf


	memFile = HttpContext.Current.Request.Files(arr1(loop1))


	strFname = memFile.FileName


	strResult &= "File Name: " & strFname & vbCrLf

	If Not System.IO.Directory.Exists(Server.MapPath("..\Documents\Accounts\Files\") & id & "\") Then
    System.IO.Directory.CreateDirectory(Server.MapPath("..\Documents\Accounts\Files\") & id & "\")
	End If

	memFile.SaveAs(Server.MapPath("..\Documents\Accounts\Files\") & id & "\" &
	System.IO.Path.GetFileName(strFname))


	strParams = Split(strFname, vbTab)


	Next loop1


	For Each strParam In strParams
	strResult &= vbCrLf & strParam
	Next

		
		
End Sub







<WebMethod> _
Public Sub saveMessageFile()

		
	Dim loop1 As Integer
	Dim arr1() As String
	Dim Files As HttpFileCollection
	Dim memFile As HttpPostedFile
	Dim bytReq(0) As Byte


	Dim intLen As Integer


	Dim strParam As String


	Files = HttpContext.Current.Request.Files 
  Dim id As String = HttpContext.Current.Request.Form("guid")








	arr1 = Files.AllKeys 


	Dim strResult As String


	Dim strFname As String


	Dim strParams() As String


	For loop1 = 0 To arr1.GetUpperBound(0)


	strResult &= "File control: " & Server.HtmlEncode(arr1(loop1)) & vbCrLf


	memFile = HttpContext.Current.Request.Files(arr1(loop1))


	strFname = memFile.FileName





	


  Try


					Dim sConnString As String = ConfigurationManager.ConnectionStrings("PortalConn").ConnectionString
					Dim sqlConn As SqlConnection = New SqlConnection(sConnString)

				Dim sQueryString As String = "INSERT INTO tblMessageFile " & _
				"(guid, " & _
				"fileName)" & _
				" VALUES " & _
				"('" & Replace(id, "'", "''") & _
				"', '" & Replace(strFname, "'", "''") & "')"

				sqlConn.Open()
				Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
				DBCommand.ExecuteNonQuery()

				sqlConn.Close()

			Catch ex As Exception

				'sqlConn.Close()
				'sqlConn.Dispose()
			End Try


	strResult &= "File Name: " & strFname & vbCrLf

	If Not System.IO.Directory.Exists(Server.MapPath("..\Documents\Messages\Files\") & id & "\") Then
    System.IO.Directory.CreateDirectory(Server.MapPath("..\Documents\Messages\Files\") & id & "\")
	End If

	memFile.SaveAs(Server.MapPath("..\Documents\Messages\Files\") & id & "\" &
	System.IO.Path.GetFileName(strFname))


	strParams = Split(strFname, vbTab)


	Next loop1


	For Each strParam In strParams
	strResult &= vbCrLf & strParam
	Next

		
		
End Sub




<WebMethod>
<ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Json)>
Public Sub GetPatientByMRN(MedicalRecordNum As String)


        Dim js As JavaScriptSerializer = New JavaScriptSerializer()


	'Dim oList As New List(Of PC_Listing)
	'Dim xmlData As New List(Of String)
	'Dim doc As New XmlDocument()

	Dim xmlString As String = ""
	Try
		Dim sConnString As String = ConfigurationManager.ConnectionStrings("TestConn").ConnectionString
		Dim sqlConn As SqlConnection = New SqlConnection(sConnString)
		Dim sQueryString As String = "SELECT * FROM [EWLC].[dbo].[Patients] where MedicalRecordNum = '" & MedicalRecordNum & "'"
		Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
		sqlConn.Open()

		Try

			Dim sqlReader As SqlDataReader = DBCommand.ExecuteReader(CommandBehavior.CloseConnection)
			Dim i As Integer
			While sqlReader.Read()
				'Dim oItem As New PC_Listing

				xmlString += "{"


				i += 1
				xmlString += """FirstName"":""" & Trim(sqlReader("FirstName").ToString) & ""","
				xmlString += """LastName"":""" & Trim(sqlReader("LastName").ToString) & ""","
				xmlString += """Gender"":""" & Trim(sqlReader("Gender").ToString) & ""","
				xmlString += """DateOfBirth"":""" & Trim(sqlReader("DateOfBirth").ToString) & ""","
				xmlString += """PhoneNum"":""" & Trim(sqlReader("PhoneNum").ToString) & ""","
				xmlString += """Address1"":""" & Trim(sqlReader("Address1").ToString) & ""","
				xmlString += """Address2"":""" & Trim(sqlReader("Address2").ToString) & ""","
				xmlString += """City"":""" & Trim(sqlReader("City").ToString) & ""","
				xmlString += """State"":""" & Trim(sqlReader("State").ToString) & ""","
				xmlString += """County"":""" & Trim(sqlReader("County").ToString) & ""","
				xmlString += """ZipCode"":""" & Trim(sqlReader("ZipCode").ToString) & ""","
				xmlString += """EmailAddress"":""" & Trim(sqlReader("EmailAddress").ToString) & ""","
				xmlString += """DateAdded"":""" & Trim(sqlReader("DateAdded").ToString) & """},"
			End While





			sqlReader.Close()

		Catch ex As Exception

			sqlConn.Close()

		End Try

		sqlConn.Close()

	Catch ex As Exception

	End Try
	xmlString = xmlString.Trim().Substring(0, xmlString.Length - 1)
	xmlString = "[" & xmlString & "]"



	HttpContext.Current.Response.BufferOutput = True
	HttpContext.Current.Response.ContentType = "application/json"
    Context.Response.Write(xmlString)
End Sub






<WebMethod>
<ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Json)>
Public Sub GetPatientMedicationListUpdatesByMRN(MedicalRecordNum As String)


        Dim js As JavaScriptSerializer = New JavaScriptSerializer()


	'Dim oList As New List(Of PC_Listing)
	'Dim xmlData As New List(Of String)
	'Dim doc As New XmlDocument()

	Dim xmlString As String = ""
	Try
		Dim sConnString As String = ConfigurationManager.ConnectionStrings("TestConn").ConnectionString
		Dim sqlConn As SqlConnection = New SqlConnection(sConnString)
		Dim sQueryString As String = "SELECT * FROM [EWLC].[dbo].[PatientMedicationListUpdates] where MedicalRecordNum = '" & MedicalRecordNum & "'"
		Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
		sqlConn.Open()

		Try

			Dim sqlReader As SqlDataReader = DBCommand.ExecuteReader(CommandBehavior.CloseConnection)
			Dim i As Integer
			While sqlReader.Read()
				'Dim oItem As New PC_Listing

				xmlString += "{"


				i += 1
				xmlString += """MedicalRecordNum"":""" & Trim(sqlReader("MedicalRecordNum").ToString) & ""","
				xmlString += """MedicationListDate"":""" & Trim(sqlReader("MedicationListDate").ToString) & ""","
				xmlString += """Medication"":""" & Trim(sqlReader("Medication").ToString) & ""","
				xmlString += """UpdateDate"":""" & Trim(sqlReader("UpdateDate").ToString) & ""","
				xmlString += """UpdateData"":""" & Trim(sqlReader("UpdateData").ToString) & ""","
				xmlString += """RecordType"":""" & Trim(sqlReader("RecordType").ToString) & """},"
			End While





			sqlReader.Close()

		Catch ex As Exception

			sqlConn.Close()

		End Try

		sqlConn.Close()

	Catch ex As Exception

	End Try
	xmlString = xmlString.Trim().Substring(0, xmlString.Length - 1)
	xmlString = "[" & xmlString & "]"



	HttpContext.Current.Response.BufferOutput = True
	HttpContext.Current.Response.ContentType = "application/json"
    Context.Response.Write(xmlString)
End Sub





<WebMethod>
<ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Json)>
Public Sub GetPatientHistoryByMRN(ByVal MedicalRecordNum As String)

	'Dim oList As New List(Of PC_Listing)
	'Dim xmlData As New List(Of String)
	'Dim doc As New XmlDocument()

	Dim xmlString As String = ""
	Try
		Dim sConnString As String = ConfigurationManager.ConnectionStrings("TestConn").ConnectionString
		Dim sqlConn As SqlConnection = New SqlConnection(sConnString)
		Dim sQueryString As String = "SELECT * FROM [EWLC].[dbo].[PatientHistory] where MedicalRecordNum = '" & MedicalRecordNum & "'"
		Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
		sqlConn.Open()

		Try

			Dim sqlReader As SqlDataReader = DBCommand.ExecuteReader(CommandBehavior.CloseConnection)
			Dim i As Integer
			While sqlReader.Read()
				'Dim oItem As New PC_Listing

				xmlString += "{"


				i += 1
				xmlString += """MedicalRecordNum"":""" & Trim(sqlReader("MedicalRecordNum").ToString) & ""","
				xmlString += """Hypertension"":""" & Trim(sqlReader("Hypertension").ToString) & ""","
				xmlString += """HeartAttack"":""" & Trim(sqlReader("HeartAttack").ToString) & ""","
				xmlString += """DiabetesMellitus"":""" & Trim(sqlReader("DiabetesMellitus").ToString) & ""","
				xmlString += """Hypothyroid"":""" & Trim(sqlReader("Hypothyroid").ToString) & ""","
				xmlString += """Reflux"":""" & Trim(sqlReader("Reflux").ToString) & ""","
				xmlString += """Glaucoma"":""" & Trim(sqlReader("Glaucoma").ToString) & ""","
				xmlString += """CoronaryArteryDisease"":""" & Trim(sqlReader("CoronaryArteryDisease").ToString) & ""","
				xmlString += """Stroke"":""" & Trim(sqlReader("Stroke").ToString) & ""","
				xmlString += """HighCholesterolTriglycerides"":""" & Trim(sqlReader("HighCholesterolTriglycerides").ToString) & ""","
				xmlString += """CongestiveHeartFailure"":""" & Trim(sqlReader("CongestiveHeartFailure").ToString) & ""","
				xmlString += """Hyperthyroid"":""" & Trim(sqlReader("Hyperthyroid").ToString) & ""","
				xmlString += """MedHistOther"":""" & Trim(sqlReader("MedHistOther").ToString) & ""","
				xmlString += """Hysterectomy"":""" & Trim(sqlReader("Hysterectomy").ToString) & ""","
				xmlString += """GallbladderRemoval"":""" & Trim(sqlReader("GallbladderRemoval").ToString) & ""","
				xmlString += """CoronaryStents"":""" & Trim(sqlReader("CoronaryStents").ToString) & ""","
				xmlString += """CSection"":""" & Trim(sqlReader("CSection").ToString) & ""","
				xmlString += """TubalLigation"":""" & Trim(sqlReader("TubalLigation").ToString) & ""","
				xmlString += """Appendectomy"":""" & Trim(sqlReader("Appendectomy").ToString) & ""","
				xmlString += """CoronaryArteryBypassGraft"":""" & Trim(sqlReader("CoronaryArteryBypassGraft").ToString) & ""","
				xmlString += """OtherSurgHist"":""" & Trim(sqlReader("OtherSurgHist").ToString) & ""","
				xmlString += """OtherSurgHistText"":""" & Trim(sqlReader("OtherSurgHistText").ToString) & ""","
				xmlString += """Cigarettes"":""" & Trim(sqlReader("Cigarettes").ToString) & ""","
				xmlString += """SmokelessTobacco"":""" & Trim(sqlReader("SmokelessTobacco").ToString) & ""","
				xmlString += """AlcoholConsumptionSocialHistory"":""" & Trim(sqlReader("AlcoholConsumptionSocialHistory").ToString) & ""","
				xmlString += """IllicitDrugs"":""" & Trim(sqlReader("IllicitDrugs").ToString) & ""","
				xmlString += """IllicitDrugsText"":""" & Trim(sqlReader("IllicitDrugsText").ToString) & ""","
				xmlString += """UnwantedExcessWeight"":""" & Trim(sqlReader("UnwantedExcessWeight").ToString) & ""","
				xmlString += """Fatigue"":""" & Trim(sqlReader("Fatigue").ToString) & ""","
				xmlString += """Swelling"":""" & Trim(sqlReader("Swelling").ToString) & ""","
				xmlString += """SkinHairNailProblems"":""" & Trim(sqlReader("SkinHairNailProblems").ToString) & ""","
				xmlString += """LargePortions"":""" & Trim(sqlReader("LargePortions").ToString) & ""","
				xmlString += """SweetsSnacks"":""" & Trim(sqlReader("SweetsSnacks").ToString) & ""","
				xmlString += """CarbonatedDrinks"":""" & Trim(sqlReader("CarbonatedDrinks").ToString) & ""","
				xmlString += """DailyExercise"":""" & Trim(sqlReader("DailyExercise").ToString) & ""","
				xmlString += """MedAllergiesText"":""" & Trim(sqlReader("MedAllergiesText").ToString) & ""","
				xmlString += """ADDorADHD"":""" & Trim(sqlReader("ADDorADHD").ToString) & ""","
				xmlString += """CarotidEndarterectomy"":""" & Trim(sqlReader("CarotidEndarterectomy").ToString) & ""","
				xmlString += """HeartAttackDate"":""" & Trim(sqlReader("HeartAttackDate").ToString) & ""","
				xmlString += """StrokeDate"":""" & Trim(sqlReader("StrokeDate").ToString) & ""","
				xmlString += """AlcoholConsumptionFrequency"":""" & Trim(sqlReader("AlcoholConsumptionFrequency").ToString) & """},"
			End While


			sqlReader.Close()

		Catch ex As Exception

			sqlConn.Close()

		End Try

		sqlConn.Close()

	Catch ex As Exception

	End Try
	xmlString = xmlString.Trim().Substring(0, xmlString.Length - 1)
	xmlString = "[" & xmlString & "]"
	'xmlString = "callback([" & xmlString & "])"
	'Dim jsonStr As String = JsonConvert.SerializeXmlNode(doc)
	'jsonStr = jsonStr.Replace("," & Chr(34) & "@", "," & Chr(34))
	'jsonStr = jsonStr.Replace("{" & Chr(34) & "@", "{" & Chr(34))
	HttpContext.Current.Response.BufferOutput = True
	HttpContext.Current.Response.ContentType = "application/json"
	HttpContext.Current.Response.Write(xmlString)
	HttpContext.Current.Response.Flush()



End Sub

    
    
<WebMethod()> _
Public Sub GetPatientVisitsByMRN(ByVal MedicalRecordNum As String)

	'Dim oList As New List(Of PC_Listing)
	'Dim xmlData As New List(Of String)
	'Dim doc As New XmlDocument()

	Dim xmlString As String = ""
	Try
		Dim sConnString As String = ConfigurationManager.ConnectionStrings("TestConn").ConnectionString
		Dim sqlConn As SqlConnection = New SqlConnection(sConnString)
		Dim sQueryString As String = "SELECT * FROM [EWLC].[dbo].[PatientVisits] where MedicalRecordNum = '" & MedicalRecordNum & "'"
		Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
		sqlConn.Open()

		Try

			Dim sqlReader As SqlDataReader = DBCommand.ExecuteReader(CommandBehavior.CloseConnection)
			Dim i As Integer
			While sqlReader.Read()
				'Dim oItem As New PC_Listing

				xmlString += "{"


				i += 1
				xmlString += """MedicalRecordNum"":""" & Trim(sqlReader("MedicalRecordNum").ToString) & ""","
				xmlString += """EmployeeDate"":""" & Trim(sqlReader("EmployeeDate").ToString) & ""","
				xmlString += """Weight"":""" & Trim(sqlReader("Weight").ToString) & ""","
				xmlString += """HeightFt"":""" & Trim(sqlReader("HeightFt").ToString) & ""","
				xmlString += """HeightIn"":""" & Trim(sqlReader("HeightIn").ToString) & ""","
				xmlString += """Hips"":""" & Trim(sqlReader("Hips").ToString) & ""","
				xmlString += """Waist"":""" & Trim(sqlReader("Waist").ToString) & ""","
				xmlString += """BMI"":""" & Trim(sqlReader("BMI").ToString) & ""","
				xmlString += """BMICategory"":""" & Trim(sqlReader("BMICategory").ToString) & ""","
				xmlString += """BPSystolic"":""" & Trim(sqlReader("BPSystolic").ToString) & ""","
				xmlString += """BPDiastolic"":""" & Trim(sqlReader("BPDiastolic").ToString) & ""","
				xmlString += """Pulse"":""" & Trim(sqlReader("Pulse").ToString) & ""","
				xmlString += """PTInitials"":""" & Trim(sqlReader("PTInitials").ToString) & ""","
				xmlString += """PTInitialsDate"":""" & Trim(sqlReader("PTInitialsDate").ToString) & ""","
				xmlString += """ClinicianDate"":""" & Trim(sqlReader("ClinicianDate").ToString) & ""","
				xmlString += """Phent"":""" & Trim(sqlReader("Phent").ToString) & ""","
				xmlString += """ED"":""" & Trim(sqlReader("ED").ToString) & ""","
				xmlString += """HCTZ"":""" & Trim(sqlReader("HCTZ").ToString) & ""","
				xmlString += """Thyrd"":""" & Trim(sqlReader("Thyrd").ToString) & ""","
				xmlString += """Chrom"":""" & Trim(sqlReader("Chrom").ToString) & ""","
				xmlString += """B12Lipotro"":""" & Trim(sqlReader("B12Lipotro").ToString) & ""","
				xmlString += """LVNInitials"":""" & Trim(sqlReader("LVNInitials").ToString) & ""","
				xmlString += """LVNInitialsDateTimeStamp"":""" & Trim(sqlReader("LVNInitialsDateTimeStamp").ToString) & ""","
				xmlString += """ClinicianInitials"":""" & Trim(sqlReader("ClinicianInitials").ToString) & ""","
				xmlString += """ClinicianInitialsDate"":""" & Trim(sqlReader("ClinicianInitialsDate").ToString) & ""","
				xmlString += """SupPhyInitials"":""" & Trim(sqlReader("SupPhyInitials").ToString) & ""","
				xmlString += """SupPhyInitialsDate"":""" & Trim(sqlReader("SupPhyInitialsDate").ToString) & ""","
				xmlString += """RecordState"":""" & Trim(sqlReader("RecordState").ToString) & ""","
				xmlString += """VisitType"":""" & Trim(sqlReader("VisitType").ToString) & """},"
			End While

			sqlReader.Close()

		Catch ex As Exception

			sqlConn.Close()

		End Try

		sqlConn.Close()

	Catch ex As Exception

	End Try
	xmlString = xmlString.Trim().Substring(0, xmlString.Length - 1)
	xmlString = "[" & xmlString & "]"
	'xmlString = "callback([" & xmlString & "])"
	'Dim jsonStr As String = JsonConvert.SerializeXmlNode(doc)
	'jsonStr = jsonStr.Replace("," & Chr(34) & "@", "," & Chr(34))
	'jsonStr = jsonStr.Replace("{" & Chr(34) & "@", "{" & Chr(34))
	HttpContext.Current.Response.BufferOutput = True
	HttpContext.Current.Response.ContentType = "application/x-javascript"
	HttpContext.Current.Response.Write(xmlString)
	HttpContext.Current.Response.Flush()



End Sub
    
    
    

    
        
<WebMethod()> _
Public Sub GetAllUsers()

	'Dim oList As New List(Of PC_Listing)
	'Dim xmlData As New List(Of String)
	'Dim doc As New XmlDocument()

	Dim xmlString As String = ""
	Try
		Dim sConnString As String = ConfigurationManager.ConnectionStrings("WebApp").ConnectionString
		Dim odbcConn As OdbcConnection = New OdbcConnection(sConnString)
		Dim sQueryString As String = "SELECT * FROM tblaccount  "
		Dim DBCommand As New OdbcCommand(sQueryString, odbcConn)
		odbcConn.Open()

		Try

			Dim odbcReader As OdbcDataReader = DBCommand.ExecuteReader(CommandBehavior.CloseConnection)
			Dim i As Integer
			While odbcReader.Read()
				'Dim oItem As New PC_Listing

				xmlString += "{"

				Dim ImageFile As String

				If odbcReader("ImageFile") Is Nothing Then

					ImageFile = Trim(odbcReader("ImageFile").ToString)

				End If

				i += 1
				'oItem.pId = Convert.ToInt16(odbcReader("pId"))
				xmlString += """acctNum"":""" & Convert.ToInt16(odbcReader("acctNum")) & ""","
				xmlString += """uName"":""" & Trim(odbcReader("uName").ToString) & ""","
				xmlString += """passWord"":""" & Trim(odbcReader("passWord").ToString) & ""","
				xmlString += """PermissionLevel"":""" & Trim(odbcReader("PermissionLevel").ToString) & ""","
				xmlString += """fullName"":""" & Trim(odbcReader("fullName").ToString) & ""","
				xmlString += """addDate"":""" & Trim(odbcReader("addDate").ToString) & ""","
				xmlString += """ImageFile"":""" & Trim(odbcReader("ImageFile").ToString) & """},"
			End While

			odbcReader.Close()

		Catch ex As Exception

			odbcConn.Close()

		End Try

		odbcConn.Close()

	Catch ex As Exception

	End Try
	xmlString = xmlString.Trim().Substring(0, xmlString.Length - 1)
	xmlString = "[" & xmlString & "]"
	'xmlString = "callback([" & xmlString & "])"
	'Dim jsonStr As String = JsonConvert.SerializeXmlNode(doc)
	'jsonStr = jsonStr.Replace("," & Chr(34) & "@", "," & Chr(34))
	'jsonStr = jsonStr.Replace("{" & Chr(34) & "@", "{" & Chr(34))
	HttpContext.Current.Response.BufferOutput = True
	HttpContext.Current.Response.ContentType = "application/x-javascript"
	HttpContext.Current.Response.Write(xmlString)
	HttpContext.Current.Response.Flush()



End Sub
    

Public Function GetHash(acctNum As String) As String
  	
   	Dim password As String = ""
	Try
		Dim sConnString As String = ConfigurationManager.ConnectionStrings("WebApp").ConnectionString
		Dim odbcConn As OdbcConnection = New OdbcConnection(sConnString)
		Dim sQueryString As String = "SELECT * FROM tblaccount WHERE acctNum = " & acctNum
		Dim DBCommand As New OdbcCommand(sQueryString, odbcConn)
		odbcConn.Open()

		Try

			Dim odbcReader As OdbcDataReader = DBCommand.ExecuteReader(CommandBehavior.CloseConnection)
			While odbcReader.Read()
	


				password = Trim(odbcReader("passWord").ToString)
				
				
			End While

			odbcReader.Close()

		Catch ex As Exception

			odbcConn.Close()

		End Try

		odbcConn.Close()

	Catch ex As Exception

	End Try
	
	Return password
End Function
    
    
<WebMethod> _
Public Sub updateUser(models As String)
    
    Dim x As Object = JsonConvert.DeserializeObject(of Object)(models)
	Dim acctNum = x(0)("acctNum")
	Dim Name = x(0)("uName")
	Dim Password = x(0)("passWord")
	Dim PermissionLevel = x(0)("PermissionLevel")
	Dim fullName = x(0)("fullName")
	Dim addDate = x(0)("addDate")
	Dim ImageFile = x(0)("ImageFile")
	
	
	Dim hash As String = GetHash(acctNum)
	
	Dim sConnString As String = ConfigurationManager.ConnectionStrings("WebApp").ConnectionString
	Dim odbcConn As OdbcConnection = New OdbcConnection(sConnString)

	Dim sQueryString As String
        Try
			If hash = "" Or hash = Password Then
               
                sQueryString = "UPDATE tblaccount SET " & _
                    "uName = '" & Replace(Name, "'", "''") & "', " & _
                    "PermissionLevel = '" & Replace(PermissionLevel, "'", "''") & "', " & _
                    "fullName = '" & Replace(fullName, "'", "''") & "', " & _
                    "addDate = '" & Replace(addDate, "'", "''") & "', " & _
                    "ImageFile = '" & Replace(ImageFile, "'", "''") & "' " & _
                    "WHERE acctNum = " & acctNum
               
		   	Else
              
                sQueryString = "UPDATE tblaccount SET " & _
                    "uName = '" & Replace(Name, "'", "''") & "', " & _
                    "passWord = MD5('" & Replace(Password, "'", "''") & "'), " & _
                    "PermissionLevel = '" & Replace(PermissionLevel, "'", "''") & "', " & _
                    "fullName = '" & Replace(fullName, "'", "''") & "', " & _
                    "addDate = '" & Replace(addDate, "'", "''") & "', " & _
                    "ImageFile = '" & Replace(ImageFile, "'", "''") & "' " & _
                    "WHERE acctNum = " & acctNum
               
		   	End If

            odbcConn.Open()
            Dim DBCommand As New OdbcCommand(sQueryString, odbcConn)
            DBCommand.ExecuteNonQuery()

            'iResult = 1

            odbcConn.Close()

        Catch ex As Exception

            odbcConn.Close()
            odbcConn.Dispose()

            'iResult = 0

        End Try
	
	
	HttpContext.Current.Response.BufferOutput = True
	HttpContext.Current.Response.ContentType = "application/json"
	HttpContext.Current.Response.Write(models)
	HttpContext.Current.Response.Flush()
End Sub
    
    
    
    
    
    
    
    
<WebMethod> _
Public Sub DeleteUser(models As String)



	Dim x As Object = JsonConvert.DeserializeObject(of Object)(models)
	Dim acctNum = x(0)("acctNum")


	Dim sQueryString As String
	Dim sConnString As String = ConfigurationManager.ConnectionStrings("WebApp").ConnectionString
	Dim odbcConn As OdbcConnection = New OdbcConnection(sConnString)

	Try

			sQueryString = "DELETE FROM tblaccount WHERE acctNum = " & acctNum



		odbcConn.Open()
		Dim DBCommand As New OdbcCommand(sQueryString, odbcConn)
		DBCommand.ExecuteNonQuery()

		odbcConn.Close()

	Catch ex As Exception

		odbcConn.Close()
		odbcConn.Dispose()



	End Try
	
	
	
	HttpContext.Current.Response.BufferOutput = True
	HttpContext.Current.Response.ContentType = "application/json"
	HttpContext.Current.Response.Write(models)
	HttpContext.Current.Response.Flush()

End Sub

    
    
    
    
<WebMethod> _
Public Sub AddUser(models As String)


   

	Dim x As Object = JsonConvert.DeserializeObject(of Object)(models)
	Dim acctNum = x(0)("acctNum")
	Dim Name = x(0)("uName")
	Dim Password = x(0)("passWord")
	Dim PermissionLevel = x(0)("PermissionLevel")("CategoryName")
	Dim fullName = x(0)("fullName")
	Dim addDate = x(0)("addDate")
	Dim ImageFile = x(0)("ImageFile")



	Dim TodaysDate As String = DateTime.Now.ToString("yyyy-MM-dd")



	Dim xmlString As String = ""
	xmlString += "{"
	xmlString += """acctNum"":""" & acctNum & ""","
	xmlString += """uName"":""" & Name & ""","
	xmlString += """passWord"":""" & Password & ""","
	xmlString += """PermissionLevel"":""" & PermissionLevel & ""","
	xmlString += """fullName"":""" & fullName & ""","
	xmlString += """addDate"":""" & TodaysDate & ""","
	xmlString += """ImageFile"":""" & ImageFile & """},"

	xmlString = xmlString.Trim().Substring(0, xmlString.Length - 1)
	'xmlString = "[" & xmlString & "]"
	'xmlString = "callback([" & xmlString & "])"




	Dim sConnString As String = ConfigurationManager.ConnectionStrings("WebApp").ConnectionString
	Dim odbcConn As OdbcConnection = New OdbcConnection(sConnString)
	Dim sQueryString As String

	Try



		sQueryString = "INSERT INTO tblaccount " & _
		"(uName, " & _
		"acctNum, " & _
		"passWord, " & _
		"PermissionLevel, " & _
		"fullName, " & _
		"addDate, " & _
		"ImageFile)" & _
		" VALUES " & _
		"('" & Replace(Name, "'", "''") & _
		"', '" & Replace(acctNum, "'", "''") & _
		"', MD5('" & Replace(Password, "'", "''") & _
		"'), '" & Replace(PermissionLevel, "'", "''") & _
		"', '" & Replace(fullName, "'", "''") & _
		"', '" & Replace(TodaysDate, "'", "''") & _
		"', '" & Replace(ImageFile, "'", "''") & "')"

		odbcConn.Open()
		Dim DBCommand As New OdbcCommand(sQueryString, odbcConn)
		DBCommand.ExecuteNonQuery()

		odbcConn.Close()

	Catch ex As Exception

		odbcConn.Close()
		odbcConn.Dispose()
		xmlString += ex.ToString()
	End Try

	HttpContext.Current.Response.BufferOutput = True
	HttpContext.Current.Response.ContentType = "application/json"
	HttpContext.Current.Response.Write("[" & xmlString & "]")
	HttpContext.Current.Response.Flush()

        
        
        
        
        

End Sub
    
    
    
    
    
    
    
    
    

    

    
    
    
    <WebMethod> _
Public Sub ClockInOut(userId As String, clockStatus As String)











	Dim sConnString As String = ConfigurationManager.ConnectionStrings("TestConn").ConnectionString
	Dim odbcConn As OdbcConnection = New OdbcConnection(sConnString)
	Dim sQueryString As String
	Dim retString As String

	Try



		sQueryString = "INSERT INTO [EWLC].[dbo].[TimeTracker] " & _
		"(userId, " & _
		"clockStatus)" & _
		" VALUES " & _
		"('" & Replace(userId, "'", "''") & _
		"', '" & Replace(clockStatus, "'", "''") & "')"

		odbcConn.Open()
		Dim DBCommand As New OdbcCommand(sQueryString, odbcConn)
		DBCommand.ExecuteNonQuery()

		odbcConn.Close()

	Catch ex As Exception
		retString += ex.ToString()
		odbcConn.Close()
		odbcConn.Dispose()
	End Try

	HttpContext.Current.Response.BufferOutput = True
	HttpContext.Current.Response.ContentType = "application/json"
	HttpContext.Current.Response.Write("[" & retString & "]")
	HttpContext.Current.Response.Flush()

        
        
        
        
        

End Sub

    
    

    
    
    
    
    
End Class



















