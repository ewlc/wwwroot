<%@ WebService Language="VB" Class="MapService" %>


Imports System
Imports System.IO
Imports System.Net.Mail
Imports System.Security
Imports System.Configuration
Imports System.Drawing
Imports System.Xml
Imports System.Web
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Xml.Serialization
Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.Odbc
Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.Collections.Generic
Imports Newtonsoft.Json
Imports System.Net.Http
Imports PdfSharp
Imports PdfSharp.Drawing
Imports PdfSharp.Pdf
Imports iTextSharp.text
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports iTextSharp.tool.xml
Imports UtilityLibraries


' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
 <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class MapService
    Inherits System.Web.Services.WebService
 

    
Public errorLog As String = "C:\Users\Public\Logs\submitFormData-LOG.txt"
Public EVisitPatientLog As String = "C:\Users\Public\Logs\submitFormData-EVisitPatientLOG.txt"
Public WriteTestLog As String = "C:\Users\Public\Logs\WriteTest.txt"
Public WriteGeneralErrorLog As String = "C:\Users\Public\Logs\GeneralErrors.txt"
Public DevServer As String = ConfigurationManager.ConnectionStrings("TestConn").ConnectionString
Public ProdServer As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
Public saveDirectory As String = "\\192.168.1.124\ScannedCharts\Signed Consent Forms\"


Public Function SendMedicationReviewEmail(subject As String, msg As String) As String


	Try

		Dim SmtpServer As New SmtpClient()
		Dim mail As New MailMessage()
		SmtpServer.Credentials = New _
		System.Net.NetworkCredential("errors.techknowvation@gmail.com", "p@ssw0rd4me!")
		SmtpServer.Port = 587
		SmtpServer.Host = "smtp.gmail.com"
		SmtpServer.EnableSsl = True
		mail = New MailMessage()
		mail.From = New MailAddress("noreply@techknowvation.com")
		mail.To.Add("austinrsellers@gmail.com")
		mail.CC.Add("steve.sellers@techknowvation.com")
		mail.Subject = subject
		mail.Body = msg
		SmtpServer.Send(mail)

	Catch ex As Exception

		WriteToSpecifiedLog(ex.ToString(), WriteGeneralErrorLog)
	End Try
            

End Function



Public Function WritePatientData(ByVal newPatient As Patient) As String


	Dim promotionsEmail As String = "0"
	Dim promotionsText As String = "0"
	Dim promotionsMail As String = "0"
	Dim promotionsNoThankYou As String = "0"

	Dim newPhone As String = newPatient.Phone.Replace("(","")
	newPhone = newPhone.Replace(")","")
	newPhone = newPhone.Replace(" ","-")
	newPhone = newPhone.Replace(vbCrLf,"")

	With newPatient

		If  .Promos.Contains("Email") Then
			promotionsEmail = "1"
		End If
		If  .Promos.Contains("Text") Then
			promotionsText = "1"
		End If
		If  .Promos.Contains("Mail") Then
			promotionsMail = "1"
		End If

	End With

	Dim retString As String

	Dim keyExist As String = CheckForKeyPatient(newPatient.MedicalRecordNum)

	WriteToLog(keyExist.ToString() & vbCrLf)


	


	If Not Convert.ToInt32(keyExist) > 0 Then
	 	Try

			Dim sConnString As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
			Dim sqlConn As SqlConnection = New SqlConnection(sConnString)
			Dim sQueryString As String
			

			Try


				With newPatient


					sQueryString = "INSERT INTO [EWLC].[dbo].[Patients] " & _
					"(FirstName, " & _
					"LastName, " & _
					"TranslatorFlag, " & _
					"MedicalRecordNum, " & _
					"DateAdded, " & _
					"HIPAASigDate, " & _
					"PatientStatusDate, " & _
					"PatientStatus, " & _
					"Address1, " & _
					"Address2, " & _
					"City, " & 
					"State, " & _
					"ZipCode, " & _
					"DateOfBirth, " & _
					"Gender, " & _
					"EmailAddress, " & _
					"PhoneNum, " & _
					"PromotionsEmail, " & _
					"PromotionsText, " & _
					"PromotionsMail)" & _
					" VALUES " & _
					"('" & Replace(.FirstName, "'", "''") & _
					"', '" & Replace(.LastName, "'", "''") & _
					"', " & Replace("0", "'", "''") & _
					", '" & Replace(.MedicalRecordNum, "'", "''") & _
					"', '" & Replace(GetDate(), "'", "''") & _
					"', '" & Replace(GetDate(), "'", "''") & _
					"', '" & Replace(GetDate(), "'", "''") & _
					"', '" & Replace("Active", "'", "''") & _
					"', '" & Replace(.Address1, "'", "''") & _
					"', '" & Replace(.Address2, "'", "''") & _
					"', '" & Replace(.City, "'", "''") & _
					"', '" & Replace(.State, "'", "''") & _
					"', " & Replace(.Zip, "'", "''") & _
					", '" & Replace(.DateOfBirth, "'", "''") & _
					"', '" & Replace(.Gender.Substring(0,1).ToString(), "'", "''") & _
					"', '" & Replace(.EmailAddress, "'", "''") & _
					"', '" & Replace(newPhone, "'", "''") & _
					"', '" & Replace(promotionsEmail, "'", "''") & _
					"', '" & Replace(promotionsText, "'", "''") & _
					"', '" & Replace(promotionsMail, "'", "''") & "')"

				End With



				WriteToLog(sQueryString)



				sqlConn.Open()
				Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
				DBCommand.ExecuteNonQuery()

				sqlConn.Close()

				

			Catch ex As Exception
				sqlConn.Close()
				sqlConn.Dispose()
				WriteToLog(ex.ToString())
				SendErrorEmail(ex.ToString())
			End Try


		Catch ex As Exception
			WriteToLog(ex.ToString())
			SendErrorEmail(ex.ToString())
		End Try
	Else
		'Update Statement

	    Dim sConnString As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
        Dim sqlConn As SqlConnection = New SqlConnection(sConnString)

        Dim sQueryString As String
        Try


        	With newPatient
                sQueryString = "UPDATE  [EWLC].[dbo].[Patients] SET " & _
                    "HIPAASigDate = '" & Replace(GetDate(), "'", "''") & "', " & _
                    "TranslatorFlag = " & "0" & ", " & _
                    "PatientStatus = '" & "Active" & "', " & _
                    "PatientStatusDate = '" & Replace(GetDate(), "'", "''") & "', " & _
                    "Address1 = '" & Replace(.Address1, "'", "''") & "', " & _
                    "Address2 = '" & Replace(.Address2, "'", "''") & "', " & _
                    "City = '" & Replace(.City, "'", "''") & "', " & _
                    "State = '" & Replace(.State, "'", "''") & "', " & _
                    "ZipCode = " & Replace(.Zip, "'", "''") & ", " & _
                    "Gender = '" & Replace(.Gender.Substring(0,1).ToString(), "'", "''") & "', " & _
                    "EmailAddress = '" & Replace(.EmailAddress, "'", "''") & "', " & _
                    "PhoneNum = '" & Replace(newPhone, "'", "''") & "', " & _
                    "PromotionsEmail = '" & Replace(promotionsEmail, "'", "''") & "', " & _
                    "PromotionsText = '" & Replace(promotionsText, "'", "''") & "', " & _
                    "PromotionsMail = '" & Replace(promotionsMail, "'", "''") & "' " & _
                    "WHERE MedicalRecordNum = '" & .MedicalRecordNum & "'"

          	End With


			WriteToLog(sQueryString & vbCrLf)

            sqlConn.Open()
            Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
            DBCommand.ExecuteNonQuery()
            sqlConn.Close()

          	

        Catch ex As Exception

            sqlConn.Close()
            sqlConn.Dispose()
            WriteToLog(ex.ToString())
			SendErrorEmail(ex.ToString())

        End Try

	End If



	Return retString

End Function

Public Function WritePatientHistoryData(newPatientHistory As PatientHistory, MedicalRecordNum As String)




	
	Dim keyExist As String = CheckForKeyPatientHistory(MedicalRecordNum)




	Dim heartAttackDate As String = ConvertDate(newPatientHistory.heartAttack)
	Dim strokeDate As String = ConvertDate(newPatientHistory.strokeDate)



	Dim highBloodPressure As String = "0"
	Dim heartAttack As String = "0"
	Dim diabetesMellitus As String = "0"
	Dim hypothyroid As String = "0"
	Dim hyperthyroid As String = "0"
	Dim reflux As String = "0"
	Dim glaucoma As String = "0"
	Dim coronaryArteryDisease As String = "0"
	Dim stroke As String = "0"
	Dim highCholesterolTriglycerides As String = "0"
	Dim congestiveHeartFailure As String = "0"
	Dim addAdhd As String = "0"
	Dim otherMedHist As String = "0"
	'-----'
	Dim hysterectomy As String = "0"
	Dim tubalLigation As String = "0"
	Dim gallbladderRemoval As String = "0"
	Dim appendectomy As String = "0"
	Dim coronaryStents As String = "0"
	Dim coronaryArteryBypassGraft As String = "0"
	Dim carotidEndarterectomy As String = "0"
	Dim cSection As String = "0"
	Dim otherSurgHist As String = "0"
	'-------'
	Dim cigarettes As String = "0"
	Dim smokelessTobacco As String = "0"
	Dim alcoholConsumption As String = "0"
	Dim illicitDrugs As String = "0"
	'-------'
	Dim swelling As String = "0"
	Dim fatigue As String = "0"
	Dim skin As String = "0"
	Dim excessWeight As String = "0"
	'-------'
	Dim largePortions As String = "0"
	Dim sweetsSnacks As String = "0"
	Dim carbonatedDrinks As String = "0"
	'----'
	Dim physicalActivity As String = "0"
	'----'
	Dim alcoholFrequency As String = "0"




	With newPatientHistory

		If .curMedHistory.Contains("High Blood Pressure") Then
			highBloodPressure = "1"
		End If
		If .curMedHistory.Contains("Heart Attack") Then
			heartAttack = "1"
		End If
		If .curMedHistory.Contains("Diabetes Mellitus") Then
			diabetesMellitus = "1"
		End If
		If .curMedHistory.Contains("Hypothyroid") Then
			hypothyroid = "1"
		End If
		If .curMedHistory.Contains("Hyperthyroid") Then
			hyperthyroid = "1"
		End If
		If .curMedHistory.Contains("Reflux") Then
			reflux = "1"
		End If
		If .curMedHistory.Contains("Glaucoma") Then
			glaucoma = "1"
		End If
		If .curMedHistory.Contains("Coronary Artery Disease") Then
			coronaryArteryDisease = "1"
		End If
		If .curMedHistory.Contains("Stroke") Then
			stroke = "1"
		End If
		If .curMedHistory.Contains("High Cholesterol/Triglycerides") Then
			highCholesterolTriglycerides = "1"
		End If
		If .curMedHistory.Contains("Congestive Heart Failure") Then
			congestiveHeartFailure = "1"
		End If
		If .curMedHistory.Contains("ADD/ADHD") Then
			addAdhd = "1"
		End If
		If .curMedHistory.Contains("Other (Please explain):") Then
			otherMedHist = "1"
		End If
		'-------------'
		If .surgHistory.Contains("Hysterectomy") Then
			hysterectomy = "1"
		End If
		If .surgHistory.Contains("Tubal Ligation") Then
			tubalLigation = "1"
		End If
		If .surgHistory.Contains("Gallbladder Removal") Then
			gallbladderRemoval = "1"
		End If
		If .surgHistory.Contains("Appendectomy") Then
			appendectomy = "1"
		End If
		If .surgHistory.Contains("Coronary Stents") Then
			coronaryStents = "1"
		End If
		If .surgHistory.Contains("Coronary Artery Bypass Graft") Then
			coronaryArteryBypassGraft = "1"
		End If
		If .surgHistory.Contains("Carotid Endarterectomy") Then
			carotidEndarterectomy = "1"
		End If
		If .surgHistory.Contains("C-Section") Then
			cSection = "1"
		End If
		If .surgHistory.Contains("Other") Then
			otherSurgHist = "1"
		End If
		'----------'
		If .socialHistory.Contains("Cigarettes") Then
			cigarettes = "1"
		End If
		If .socialHistory.Contains("Smokeless Tobacco") Then
			smokelessTobacco = "1"
		End If
		If .socialHistory.Contains("Alcohol Consumption") Then
			alcoholConsumption = "1"
		End If
		If .socialHistory.Contains("Illicit Drugs:") Then
			illicitDrugs = "1"
		End If
		'-----'
		If .revSymptoms.Contains("Swelling") Then
			swelling = "1"
		End If
		If .revSymptoms.Contains("Fatigue") Then
			fatigue = "1"
		End If
		If .revSymptoms.Contains("Skin") Then
			skin = "1"
		End If
		If .revSymptoms.Contains("Excess Weight") Then
			excessWeight = "1"
		End If
		'-----'
		If .eatHabits.Contains("Large Portions") Then
			largePortions = "1"
		End If
		If .eatHabits.Contains("Sweets/Snacks") Then
			sweetsSnacks = "1"
		End If
		If .eatHabits.Contains("Carbonated Drinks") Then
			carbonatedDrinks = "1"
		End If
		'-------'
		If .physActivity.Contains("Walk or Other") Then
			physicalActivity = "1"
		End If
		'----'

		If .alcoholConsumption.Contains("Infrequent Social Drinking") Then
			alcoholFrequency = "1"
		End If
		If .alcoholConsumption.Contains("Frequent Small Amounts") Then
			alcoholFrequency = "2"
		End If
		If .alcoholConsumption.Contains("Frequent Large Amounts") Then
			alcoholFrequency = "3"
		End If














	End With

	Dim retString As String


	WriteToLog(keyExist.ToString() & vbCrLf)


	


	If Not Convert.ToInt32(keyExist) > 0 Then
	 	Try

			Dim sConnString As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
			Dim sqlConn As SqlConnection = New SqlConnection(sConnString)
			Dim sQueryString As String
			

			Try


				With newPatientHistory

					sQueryString = "INSERT INTO [EWLC].[dbo].[PatientHistory] " & _
					"(HeartAttack, " & _
					"HeartAttackDate, " & _
					"StrokeDate, " & _
					"CarotidEndarterectomy, " & _
					"MedicalRecordNum, " & _
					"Hypertension, " & _
					"DiabetesMellitus, " & _
					"Hypothyroid, " & _
					"Reflux, " & _
					"Glaucoma, " & _
					"CoronaryArteryDisease, " & _
					"Stroke, " & _
					"HighCholesterolTriglycerides, " & _
					"CongestiveHeartFailure, " & _
					"Hyperthyroid, " & 
					"MedHistOther, " & _
					"MedHistOtherText, " & _
					"OtherSurgHistText, " & _
					"Hysterectomy, " & _
					"GallbladderRemoval, " & _
					"CoronaryStents, " & _
					"CSection, " & _
					"TubalLigation, " & _
					"Appendectomy, " & _
					"CoronaryArteryBypassGraft, " & _
					"OtherSurgHist, " & _
					"Cigarettes, " & _
					"SmokelessTobacco, " & _
					"AlcoholConsumptionSocialHistory, " & _
					"IllicitDrugs, " & _
					"IllicitDrugsText, " & _
					"UnwantedExcessWeight, " & _
					"Fatigue, " & _
					"Swelling, " & _
					"SkinHairNailProblems, " & _
					"LargePortions, " & _
					"SweetsSnacks, " & _
					"CarbonatedDrinks, " & _
					"DailyExercise, " & _
					"MedAllergiesText, " & _
					"ADDorADHD, " & _
					"AlcoholConsumptionFrequency)" & _
					" VALUES " & _
					"('" & Replace(heartAttack, "'", "''") & _
					"', '" & Replace(heartAttackDate, "'", "''") & _
					"', '" & Replace(strokeDate, "'", "''") & _
					"', '" & Replace(carotidEndarterectomy, "'", "''") & _
					"', '" & Replace(MedicalRecordNum, "'", "''") & _
					"', '" & Replace(highBloodPressure, "'", "''") & _
					"', '" & Replace(diabetesMellitus, "'", "''") & _
					"', '" & Replace(hypothyroid, "'", "''") & _
					"', '" & Replace(reflux, "'", "''") & _
					"', '" & Replace(glaucoma, "'", "''") & _
					"', '" & Replace(CoronaryArteryDisease, "'", "''") & _
					"', '" & Replace(stroke, "'", "''") & _
					"', '" & Replace(highCholesterolTriglycerides, "'", "''") & _
					"', '" & Replace(congestiveHeartFailure, "'", "''") & _
					"', '" & Replace(hyperthyroid, "'", "''") & _
					"', '" & Replace(otherMedHist, "'", "''") & _
					"', '" & Replace(.medOther, "'", "''") & _
					"', '" & Replace(.other, "'", "''") & _
					"', '" & Replace(hysterectomy, "'", "''") & _
					"', '" & Replace(gallbladderRemoval, "'", "''") & _
					"', '" & Replace(coronaryStents, "'", "''") & _
					"', '" & Replace(cSection, "'", "''") & _
					"', '" & Replace(tubalLigation, "'", "''") & _
					"', '" & Replace(appendectomy, "'", "''") & _
					"', '" & Replace(CoronaryArteryBypassGraft, "'", "''") & _
					"', '" & Replace(otherSurgHist, "'", "''") & _
					"', '" & Replace(cigarettes, "'", "''") & _
					"', '" & Replace(smokelessTobacco, "'", "''") & _
					"', '" & Replace(alcoholConsumption, "'", "''") & _
					"', '" & Replace(illicitDrugs, "'", "''") & _
					"', '" & Replace(.illicitDrugs, "'", "''") & _
					"', '" & Replace(excessWeight, "'", "''") & _
					"', '" & Replace(fatigue, "'", "''") & _
					"', '" & Replace(swelling, "'", "''") & _
					"', '" & Replace(skin, "'", "''") & _
					"', '" & Replace(largePortions, "'", "''") & _
					"', '" & Replace(sweetsSnacks, "'", "''") & _
					"', '" & Replace(carbonatedDrinks, "'", "''") & _
					"', '" & Replace(physicalActivity, "'", "''") & _
					"', '" & Replace(.allergiesToMeds, "'", "''") & _
					"', '" & Replace(addAdhd, "'", "''") & _
					"', '" & Replace(alcoholFrequency, "'", "''") & "')"
					

				End With
				

				WriteToLog(sQueryString)
				

				sqlConn.Open()
				Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
				DBCommand.ExecuteNonQuery()

				sqlConn.Close()

				

			Catch ex As Exception
				sqlConn.Close()
				sqlConn.Dispose()
				WriteToLog(ex.ToString())
				SendErrorEmail(ex.ToString())
			End Try


		Catch ex As Exception
			WriteToLog(ex.ToString())
			SendErrorEmail(ex.ToString())
		End Try
	Else
		'Update Statement

	    Dim sConnString As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
        Dim sqlConn As SqlConnection = New SqlConnection(sConnString)

        Dim sQueryString As String
        Try


        	With newPatientHistory




                sQueryString = "UPDATE  [EWLC].[dbo].[PatientHistory] SET " & _
                    "HeartAttack = '" & Replace(heartAttack, "'", "''") & "', " & _
                    "HeartAttackDate = '" & Replace(heartAttackDate, "'", "''") & "', " & _
                    "StrokeDate = '" & Replace(strokeDate, "'", "''") & "', " & _
                    "CarotidEndarterectomy = '" & Replace(carotidEndarterectomy, "'", "''") & "', " & _
                    "Hypertension = '" & Replace(highBloodPressure, "'", "''") & "', " & _
                    "DiabetesMellitus = '" & Replace(diabetesMellitus, "'", "''") & "', " & _
                    "Hypothyroid = '" & Replace(hypothyroid, "'", "''") & "', " & _
                    "Reflux = '" & Replace(reflux, "'", "''") & "', " & _
                    "Glaucoma = '" & Replace(glaucoma, "'", "''") & "', " & _
                    "CoronaryArteryDisease = " & Replace(coronaryArteryDisease, "'", "''") & ", " & _
                    "Stroke = '" & Replace(stroke, "'", "''") & "', " & _
                    "HighCholesterolTriglycerides = '" & Replace(highCholesterolTriglycerides, "'", "''") & "', " & _
                    "CongestiveHeartFailure = '" & Replace(congestiveHeartFailure, "'", "''") & "', " & _
                    "Hyperthyroid = '" & Replace(hyperthyroid, "'", "''") & "', " & _
                    "MedHistOther = '" & Replace(otherMedHist, "'", "''") & "', " & _
                    "MedHistOtherText = '" & Replace(.medOther, "'", "''") & "', " & _
                    "OtherSurgHistText = '" & Replace(.other, "'", "''") & "', " & _
                    "Hysterectomy = '" & Replace(hysterectomy, "'", "''") & "', " & _
                    "GallbladderRemoval = '" & Replace(gallbladderRemoval, "'", "''") & "', " & _
                    "CoronaryStents = '" & Replace(coronaryStents, "'", "''") & "', " & _
                    "CSection = '" & Replace(cSection, "'", "''") & "', " & _
                    "TubalLigation = '" & Replace(tubalLigation, "'", "''") & "', " & _
                    "Appendectomy = '" & Replace(appendectomy, "'", "''") & "', " & _
                    "CoronaryArteryBypassGraft = '" & Replace(CoronaryArteryBypassGraft, "'", "''") & "', " & _
                    "OtherSurgHist = '" & Replace(otherSurgHist, "'", "''") & "', " & _
                    "Cigarettes = '" & Replace(cigarettes, "'", "''") & "', " & _
                    "SmokelessTobacco = '" & Replace(smokelessTobacco, "'", "''") & "', " & _
                    "AlcoholConsumptionSocialHistory = '" & Replace(alcoholConsumption, "'", "''") & "', " & _
                    "IllicitDrugs = '" & Replace(illicitDrugs, "'", "''") & "', " & _
                    "IllicitDrugsText = '" & Replace(.illicitDrugs, "'", "''") & "', " & _
                    "UnwantedExcessWeight = '" & Replace(excessWeight, "'", "''") & "', " & _
                    "Fatigue = '" & Replace(fatigue, "'", "''") & "', " & _
                    "Swelling = '" & Replace(swelling, "'", "''") & "', " & _
                    "SkinHairNailProblems = '" & Replace(skin, "'", "''") & "', " & _
                    "LargePortions = '" & Replace(largePortions, "'", "''") & "', " & _
                    "SweetsSnacks = '" & Replace(sweetsSnacks, "'", "''") & "', " & _
                    "CarbonatedDrinks = '" & Replace(carbonatedDrinks, "'", "''") & "', " & _
                    "DailyExercise = '" & Replace(physicalActivity, "'", "''") & "', " & _
                    "MedAllergiesText = '" & Replace(.allergiesToMeds, "'", "''") & "', " & _
                    "ADDorADHD = '" & Replace(addAdhd, "'", "''") & "', " & _
                    "AlcoholConsumptionFrequency = '" & Replace(alcoholFrequency, "'", "''") & "' " & _
                    "WHERE MedicalRecordNum = '" & MedicalRecordNum & "'"



          	End With

			WriteToLog(sQueryString & vbCrLf)

            sqlConn.Open()
            Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
            DBCommand.ExecuteNonQuery()
            sqlConn.Close()

          	

        Catch ex As Exception

            sqlConn.Close()
            sqlConn.Dispose()
            WriteToLog(ex.ToString())
			SendErrorEmail(ex.ToString())

        End Try

	End If




End Function



Public Function WriteMedicationListUpdate(MedicalRecordNum As String, curMeds As String)

		

		Dim retBoolean As String = CheckForMedication(MedicalRecordNum, curMeds)
		WriteToLog(retBoolean)

		If Not Convert.ToInt32(retBoolean) > 0 Then



		 	Try

				Dim sConnString As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
				Dim sqlConn As SqlConnection = New SqlConnection(sConnString)
				Dim sQueryString As String
				

				Try


					

						sQueryString = "INSERT INTO [EWLC].[dbo].[PatientMedicationListUpdates] " & _
						"(MedicalRecordNum, " & _
						"MedicationListDate, " & _
						"Medication, " & _
						"RecordType)" & _
						" VALUES " & _
						"('" & Replace(MedicalRecordNum, "'", "''") & _
						"', '" & Replace(GetDate(), "'", "''") & _
						"', '" & Replace(curMeds, "'", "''") & _
						"', '" & Replace("Medication", "'", "''") & "')"
						

				
					

					WriteToLog(sQueryString)
					

					sqlConn.Open()
					Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
					DBCommand.ExecuteNonQuery()

					sqlConn.Close()

					

				Catch ex As Exception
					sqlConn.Close()
					sqlConn.Dispose()
					WriteToLog(ex.ToString())
					SendErrorEmail(ex.ToString())
				End Try


			Catch ex As Exception
				WriteToLog(ex.ToString())
				SendErrorEmail(ex.ToString())
			End Try







		End If











End Function



Public Function CheckForMedication(MedicalRecordNum As String, Medication As String) As String


	Dim retBoolean As String = "0"
	Try
		Dim sConnString As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
		Dim sqlConn As SqlConnection = New SqlConnection(sConnString)
		Dim sQueryString As String = "SELECT COUNT(*) AS retBoolean FROM [EWLC].[dbo].[PatientMedicationListUpdates] where MedicalRecordNum = '" & MedicalRecordNum & "' AND Medication = '" & Replace(Medication, "'", "''") & "'"
		WriteToLog(sQueryString)
		Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
		sqlConn.Open()

		Try

			Dim sqlReader As SqlDataReader = DBCommand.ExecuteReader(CommandBehavior.CloseConnection)
			Dim i As Integer
			While sqlReader.Read()
				
				
				retBoolean = Convert.ToString(sqlReader("retBoolean"))


			End While

			sqlReader.Close()

		Catch ex As Exception

			sqlConn.Close()
			WriteToLog(vbCrLf & ex.ToString())
			SendErrorEmail(ex.ToString())
		End Try

		sqlConn.Close()

	Catch ex As Exception
		WriteToLog(vbCrLf & ex.ToString())
		SendErrorEmail(ex.ToString())
	End Try

	Return retBoolean

End Function







Public Function CheckForKeyPatientHistory(MedicalRecordNum As String) As String


	Dim retBoolean As String
	Try
		Dim sConnString As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
		Dim sqlConn As SqlConnection = New SqlConnection(sConnString)
		Dim sQueryString As String = "SELECT COUNT(*) AS retBoolean FROM [EWLC].[dbo].[PatientHistory] where MedicalRecordNum = '" & MedicalRecordNum & "' "
		Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
		sqlConn.Open()

		Try

			Dim sqlReader As SqlDataReader = DBCommand.ExecuteReader(CommandBehavior.CloseConnection)
			Dim i As Integer
			While sqlReader.Read()
				
				
				retBoolean = Convert.ToString(sqlReader("retBoolean"))


			End While

			sqlReader.Close()

		Catch ex As Exception

			sqlConn.Close()
			WriteToLog(vbCrLf & ex.ToString())
			SendErrorEmail(ex.ToString())
		End Try

		sqlConn.Close()

	Catch ex As Exception
		WriteToLog(vbCrLf & ex.ToString())
		SendErrorEmail(ex.ToString())
	End Try

	Return retBoolean

End Function





Public Function CheckForKeyPatient(MedicalRecordNum As String) As String


	Dim retBoolean As String
	Try
		Dim sConnString As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
		Dim sqlConn As SqlConnection = New SqlConnection(sConnString)
		Dim sQueryString As String = "SELECT COUNT(*) AS retBoolean FROM [EWLC].[dbo].[Patients] where MedicalRecordNum = '" & MedicalRecordNum & "' "
		Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
		sqlConn.Open()

		Try

			Dim sqlReader As SqlDataReader = DBCommand.ExecuteReader(CommandBehavior.CloseConnection)
			Dim i As Integer
			While sqlReader.Read()
				
				
				retBoolean = Convert.ToString(sqlReader("retBoolean"))


			End While

			sqlReader.Close()

		Catch ex As Exception

			sqlConn.Close()
			WriteToLog(vbCrLf & ex.ToString())
			SendErrorEmail(ex.ToString())
		End Try

		sqlConn.Close()

	Catch ex As Exception
		WriteToLog(vbCrLf & ex.ToString())
		SendErrorEmail(ex.ToString())
	End Try

	Return retBoolean

End Function






Public Function GetDate() As String

	Dim myDateString as String = Date.Today.ToString()
	Dim myDate as DateTime
	DateTime.TryParse(myDateString, myDate).ToString()

	Return myDate.ToString("yyyy-MM-dd")

End Function

Public Function GetDateTime() As String

	Dim myDateString as String = Date.Today.ToString()
	Dim myDate as DateTime
	DateTime.TryParse(myDateString, myDate).ToString()

	Return myDate.ToString("yyyy-MM-dd hh:mm:ss")

End Function

Public Function WriteToSpecifiedLog(content As String, dir As String)
	Try

		'Dim strFile As String = "C:\Users\Public\submitFormData-LOG.txt"
		Dim curDate As String = GetDate()
		'Dim strFile As String = dir & filename
		'Dim fileExists As Boolean = File.Exists(strFile)


		'Scontent = curDate & " : " & content
  		File.AppendAllText(dir, content & vbCrLf)






        Try
            'Dim SmtpServer As New SmtpClient()
            'Dim mail As New MailMessage()
            'SmtpServer.Credentials = New _
			'Net.NetworkCredential("errors.techknowvation@gmail.com", "p@ssw0rd4me!")
            'SmtpServer.Port = 587
            'SmtpServer.Host = "smtp.gmail.com"
            'SmtpServer.EnableSsl = True
            'mail = New MailMessage()
            'mail.From = New MailAddress("noreply@techknowvation.com")
            'mail.To.Add("austinrsellers@gmail.com")
            'mail.Subject = "Error"
            'mail.Body = content
            'SmtpServer.Send(mail)
        Catch ex As Exception
        	'WriteToLog(ex.ToString())
			'SendErrorEmail(ex.ToString())
        End Try



	Catch ex As Exception
	   	File.AppendAllText(EVisitPatientLog, ex.ToString())
		SendErrorEmail(ex.ToString())
	End Try
End Function




Public Function WriteToLog(content As String)
	Try

		'Dim strFile As String = "C:\Users\Public\submitFormData-LOG.txt"
		Dim curDate As String = GetDate()
		'Dim strFile As String = dir & filename
		'Dim fileExists As Boolean = File.Exists(strFile)


		'Scontent = curDate & " : " & content
  		File.AppendAllText(errorLog, content & vbCrLf)






        Try
            'Dim SmtpServer As New SmtpClient()
            'Dim mail As New MailMessage()
            'SmtpServer.Credentials = New _
			'Net.NetworkCredential("errors.techknowvation@gmail.com", "p@ssw0rd4me!")
            'SmtpServer.Port = 587
            'SmtpServer.Host = "smtp.gmail.com"
            'SmtpServer.EnableSsl = True
            'mail = New MailMessage()
            'mail.From = New MailAddress("noreply@techknowvation.com")
            'mail.To.Add("austinrsellers@gmail.com")
            'mail.Subject = "Error"
            'mail.Body = content
            'SmtpServer.Send(mail)
        Catch ex As Exception
        	WriteToLog(ex.ToString())
			SendErrorEmail(ex.ToString())
        End Try



	Catch ex As Exception
	   	File.AppendAllText(errorLog, ex.ToString())
		SendErrorEmail(ex.ToString())
	End Try
End Function




Public Function GenerateID(ByVal newPatient As Patient)

	'First 4 of Last name (IF LASTNAME < 4 THEN FILL WITH 'X')



	'newPatient.FirstName = Trim(newPatient.FirstName)
	'newPatient.LastName = Trim(newPatient.LastName)


	Dim gFirstName As String
	Dim gLastName As String

	gFirstName = Trim(newPatient.FirstName.Replace(" ",""))
	gLastName = Trim(newPatient.LastName.Replace(" ",""))




	gFirstName = Regex.Replace(gFirstName,"[^a-zA-Z0-9]","")
	gLastName = Regex.Replace(gLastName,"[^a-zA-Z0-9]","")

	Dim Sector1 As String
	Dim nLen As Integer
	If gLastName.Trim.Length < 4 Then
		Sector1 = gLastName.Trim.ToUpper()

		For index As Integer = 1 To 4
    		Sector1 += "X"
			If Sector1.Trim.Length = 4 Then
				Exit For
			End If
		Next

	Else
		Sector1 = gLastName.Substring(0,4).ToUpper()
	End If


	'First Letter Of Firstname
	Dim Sector2 As String = gFirstName.Substring(0,1).ToUpper()


	'
	Dim Sector3 As String
	Dim myDateString as String = newPatient.DateOfBirth
	Dim myDate as DateTime
	DateTime.TryParse(myDateString, myDate).ToString()
	Sector3 += myDate.ToString("MMddyy")	



	Dim retString As String = Sector1 & Sector2 & Sector3


	Return retString


End Function



<WebMethod()> _
Public Sub submitFormData()

	WriteToLog(GetDateTime() & " : ")
	Dim retLog As String


	Dim buffer As Byte() = New Byte(context.Request.InputStream.Length - 1) {}
	context.Request.InputStream.Read(buffer, 0, buffer.Length)
	context.Request.InputStream.Position = 0




    Dim jData As String
	Using reader As New StreamReader(HttpContext.Current.Request.InputStream)
		jData += reader.ReadToEnd()
	End Using

	WriteToLog(vbCrLf & jData)

	jData = jData.Replace("null", Chr(34) & Chr(34))

	Dim x As Object = JsonConvert.DeserializeObject(of Object)(jData)


	Dim retString As String 
	Dim logStr As String
	Dim xmlString As String


	Try

		Dim FormID = x("FormID")
		Dim UniqueID = x("UniqueID") 
		Dim patientRegistration = x("Patient Registration")
		Dim Location = x("Location")
		Dim prefClinic = x("Please indicate your preferred clinic")
		Dim newPatReg = x("New Patient Registration Form")
		Dim firstName = x("Patient Name")("first")
		Dim lastName = x("Patient Name")("last")
		Dim Address1 = x("Address")("address")
		Dim Address2 = x("Address")("address2")
		Dim City = x("Address")("city")
		Dim State = x("Address")("state")
		Dim Zip = x("Address")("zip")
		Dim dob = x("Date of Birth")
		Dim Gender = x("Gender")
		Dim EmailAddress = x("Email Address")
		Dim Phone = x("Phone")

		Dim Promos = ""
		If jData.Contains("If you wish to receive any information or promotions from Express Weight Loss Clinic:") Then
			Promos = x("If you wish to receive any information or promotions from Express Weight Loss Clinic:").ToString()
		End If
		

		Dim medHistory = x("Medical History")
		Dim curMedHistory = x("Current/Past Medical History.  Check all that apply:").ToString()
		Dim heartAttack = x("Heart Attack - Date of Last Occurrence")
		Dim strokeDate = x("Stroke - Date of Last Occurrence")

		Dim medOther = ""
		If jData.Contains("Medical History Other (Please explain):") Then
			medOther = x("Medical History Other (Please explain):").ToString()
		End If



		Dim other = x("Past Surgery Other (Please explain):")
		Dim surgHistory = x("Past Surgical History.  Check all that apply:").ToString()
		Dim socialHistory = x("Social History.  Check all that apply:").ToString()
		Dim alcoholConsumption = x("Alcohol Consumption")
		Dim illicitDrugs = x("Illicit Drugs:")
		Dim revSymptoms = x("Review of Symptoms.  Check all that apply:").ToString()
		Dim eatHabits = x("Eating Habits.  Check all that apply:").ToString()
		Dim physActivity = x("Physical Activity.  Check if applicable:")
		Dim allergiesToMeds = x("Allergies to Medications.  Please List:").ToString()
		Dim curMeds = x("Current Medications:").ToString()
		Dim Waiver = x("PATIENT WAIVER AND INFORMATIONAL CONSENT FORM")
		Dim Notice = x("Notice of Privacy Practices Form")
		Dim Acknowledgement = x("Acknowledgement")
		Dim Signature = x("Signature")
		Dim printedFirstName = x("Patient Printed Name")("first")
		Dim printedLastName = x("Patient Printed Name")("last")
		Dim todaysDate = x("Today's Date")
		Dim above18 = x("I am 18 years of age or older")
		Dim Consent = x("Consent to treat minor children")
		Dim gSig = x("Guardian's Signature")
		Dim pDob = x("Patient Date of Birth")




		Dim gPrintedName = ""
		If jData.Contains("Guardian's Printed Name") Then
			gPrintedName = x("Guardian's Printed Name").ToString()
			If Not gPrintedName = "" Then
				gPrintedName = x("Guardian's Printed Name")("first") & " " & x("Guardian's Printed Name")("last")
			End If
		End If



		Dim Thanks = x("Thank you for completing the Patient Registration. Click submit below to be routed to our forms page.")





		Dim newPatient As New Patient
		Dim newPatientHistory As New PatientHistory

		With newPatient

			.FirstName = Trim(FormatProperCase(firstName))
	        .LastName = Trim(FormatProperCase(lastName))
	        .Address1 = FormatProperCase(Address1)
	        .Address2 = FormatProperCase(Address2)
	        .City = FormatProperCase(City)
	        .State = State
	        .Zip = Zip
	        .DateOfBirth = dob
	        .Gender = Gender
	        .EmailAddress = EmailAddress
	        .Phone = Phone
	        .Promos = Promos

	    End With

		newPatient.MedicalRecordNum = GenerateID(newPatient)

		With newPatient
		
				retLog += vbCrLf & .FirstName
	            retLog += vbCrLf & .LastName
	            retLog += vbCrLf & .Address1
	            retLog += vbCrLf & .Address2
	            retLog += vbCrLf & .City
	            retLog += vbCrLf & .State
	            retLog += vbCrLf & .Zip
	            retLog += vbCrLf & .DateOfBirth
	            retLog += vbCrLf & .Gender
	            retLog += vbCrLf & .EmailAddress
	            retLog += vbCrLf & .Phone
	            retLog += vbCrLf & .Promos
	            retLog += vbCrLf & .MedicalRecordNum

	    End With

	    Dim strCurMedHistory As String = curMedHistory
	    Dim strSurgHistory As String = surgHistory
	    Dim strAllergiesToMeds As String = allergiesToMeds
	    Dim strIllicitDrugs As String = illicitDrugs
	    Dim strCurMeds As String = curMeds
	    Dim strCurMedsOther As String = medOther



	    strCurMedHistory = CleanString(strCurMedHistory)
	    strSurgHistory = CleanString(strSurgHistory)
	    strAllergiesToMeds = CleanString(strAllergiesToMeds)
	    strIllicitDrugs = CleanString(strIllicitDrugs)
	    strCurMeds = CleanString(strCurMeds)
	    strCurMedsOther = CleanString(strCurMedsOther)


	    If strCurMedHistory.Length() > 250 Then
	    	strCurMedHistory = strCurMedHistory.SubString(0, 248)
	    	strCurMedHistory = strCurMedHistory.Replace(vbCrLf, "")
	    End If

	    If strSurgHistory.Length() > 250 Then
	    	strSurgHistory = strSurgHistory.SubString(0, 248)
	    	strSurgHistory = strSurgHistory.Replace(vbCrLf, "")
	    End If

	    If strAllergiesToMeds.Length() > 250 Then
	    	strAllergiesToMeds = strAllergiesToMeds.SubString(0, 248)
	    	strAllergiesToMeds = strAllergiesToMeds.Replace(vbCrLf, "")
	    End If

	    If strIllicitDrugs.Length() > 100 Then
	    	strIllicitDrugs = strIllicitDrugs.SubString(0, 98)
	    	strIllicitDrugs = strIllicitDrugs.Replace(vbCrLf, "")
	    End If

	    If strCurMeds.Length() > 100 Then
	    	strCurMeds = strCurMeds.SubString(0, 98)
	    	strCurMeds = strCurMeds.Replace(vbCrLf, "")
	    End If

	    If strCurMedsOther.Length() > 250 Then
	    	strCurMedsOther = strCurMedsOther.SubString(0, 248)
	    	strCurMedsOther = strCurMedsOther.Replace(vbCrLf, "")
	    End If




	    With newPatientHistory


			.medHistory = medHistory
			.curMedHistory = strCurMedHistory
			.heartAttack = heartAttack
			.strokeDate = strokeDate
			.other = other
			.medOther = strCurMedsOther
			.surgHistory = strSurgHistory
			.socialHistory = socialHistory
			.alcoholConsumption = alcoholConsumption
			.illicitDrugs = strIllicitDrugs
			.revSymptoms = revSymptoms
			.eatHabits = eatHabits
			.physActivity = physActivity
			.allergiesToMeds = UppercaseFirstLetter(strAllergiesToMeds.ToLower())
			.curMeds = CheckForNull(curMeds)
			.Waiver = Waiver
			.Notice = Notice
			.Acknowledgement = Acknowledgement
			.Signature = Signature
			.printedFirstName = Trim(printedFirstName)
			.printedLastName = Trim(printedLastName)
			.todaysDate = todaysDate
			.above18 = above18
			.Consent = Consent
			.gSig = gSig
			.pDob = pDob
			.gPrintedName = gPrintedName
			.Thanks = Thanks



	    End With


	    With newPatientHistory


			retLog += vbCrLf & .medHistory
			retLog += vbCrLf & .curMedHistory
			retLog += vbCrLf & .heartAttack
			retLog += vbCrLf & .strokeDate
			retLog += vbCrLf & .other
			retLog += vbCrLf & .medOther
			retLog += vbCrLf & .surgHistory
			retLog += vbCrLf & .socialHistory
			retLog += vbCrLf & .alcoholConsumption
			retLog += vbCrLf & .illicitDrugs
			retLog += vbCrLf & .revSymptoms
			retLog += vbCrLf & .eatHabits
			retLog += vbCrLf & .physActivity
			retLog += vbCrLf & .allergiesToMeds
			retLog += vbCrLf & .curMeds
			retLog += vbCrLf & .Waiver
			retLog += vbCrLf & .Notice
			retLog += vbCrLf & .Acknowledgement
			retLog += vbCrLf & .Signature
			retLog += vbCrLf & .printedFirstName
			retLog += vbCrLf & .printedLastName
			retLog += vbCrLf & .todaysDate
			retLog += vbCrLf & .above18
			retLog += vbCrLf & .Consent
			retLog += vbCrLf & .gSig
			retLog += vbCrLf & .pDob
			retLog += vbCrLf & .gPrintedName
			'retLog += vbCrLf & .Thanks



	    End With



	    WriteToLog(retLog)




		WriteToLog("Medical Record Number - " & newPatient.MedicalRecordNum)

    	retLog += WritePatientData(newPatient)


    	retLog += WritePatientHistoryData(newPatientHistory, newPatient.MedicalRecordNum)


    	'If Not CheckForNull(strCurMeds) = "" Then

    	''	WriteMedicationListUpdate(newPatient.MedicalRecordNum, strCurMeds)

    	'End If


    	If Not CheckForNull(curMeds.ToString()) = "" Then


    		Try

	    		Dim curMedsArray As String() = curMeds.ToString().Split(new String() {Environment.NewLine}, StringSplitOptions.None)

			    For Each med In curMedsArray

			    	WriteToLog(med)
		    		If Not Trim(med) = "" Then
						If med.Length() > 99 Then
							Dim medContent As String = newPatient.FirstName & " " & newPatient.LastName & vbCrLf & newPatient.MedicalRecordNum & vbCrLf & "Medications To Examine: " & med
							'SendMedicationReviewEmail("Medication Length too long:", medContent)
							MailLibrary.Send(medContent, "New Registration Form : Medication Length too long", "austinrsellers@gmail.com", "steve.sellers@techknowvation.com,ehstyler@exphs.com")
							med = med.Substring(0,99)
						End If
						WriteMedicationListUpdate(newPatient.MedicalRecordNum, med)
						If med.Contains(",") Then
							'SendErrorEmail("Medications To Examine: " & med)
							Dim medContent As String = newPatient.FirstName & " " & newPatient.LastName & vbCrLf & newPatient.MedicalRecordNum & vbCrLf & "Medications To Examine: " & med
							'SendMedicationReviewEmail("Medication Review", medContent)
							MailLibrary.Send(medContent, "New Registration Form : Medication Review", "austinrsellers@gmail.com", "steve.sellers@techknowvation.com,ehstyler@exphs.com")
							WriteToLog("Medications To Examine: " & med)
						End If
			    	End If

			    Next

    		Catch ex As Exception
    			WriteToLog(ex.ToString())
    		End Try

			
		Else

			WriteToLog("Medications: Null")

		End If

	
		Dim sFilename As String = newPatient.MedicalRecordNum & " - " & newPatient.FirstName & " " & newPatient.LastName & " - " & "New Patient Registration"
	 	'Dim fileSaveResult As String = SaveSignatureFile(Signature, sFilename)
	 	Dim fileSaveResult As String = WriteHtmlContentNewRegistration(Signature, prefClinic, newPatient, newPatientHistory, saveDirectory, sFilename)
	 	WriteToLog("File Write Log: " & fileSaveResult)

	 	

	 	If File.Exists(saveDirectory & sFilename & ".png") Then
	 			File.Delete(saveDirectory & sFilename & ".png")
 		End If
	
    

    Catch ex As Exception

    	WriteToLog(ex.ToString)
		SendErrorEmail(ex.ToString())

    End Try 

	


	HttpContext.Current.Response.BufferOutput = True
	HttpContext.Current.Response.ContentType = "application/x-javascript"
	HttpContext.Current.Response.Write(retString)
	HttpContext.Current.Response.Flush()



End Sub






Public Function WriteHtmlContentNewRegistration(url As String, prefClinic as String, newPatient As Patient, newPatientHistory As PatientHistory, fileDir As String, filename As String) As String

		Try 


			Dim nfilename As String = fileDir & filename & ".png"
			Dim filenamePdf As String = fileDir & filename & ".pdf"


			If Not url = "" Then


				If Not File.Exists(nfilename) Then
					File.Delete(nfilename)
					My.Computer.Network.DownloadFile(url, nfilename)
				Else
					File.Delete(nfilename)
					My.Computer.Network.DownloadFile(url, nfilename)
				End If


			End If
		        

		        Dim strFileShortName As String = filename & ".pdf"
		        Dim strPngShortName As String = filename & ".png"
		        Dim strFileName As String = fileDir & strFileShortName



			



	        	Dim SampleImage = Path.Combine(fileDir, strPngShortName)






				Dim document As New Document(iTextSharp.text.PageSize.LETTER, 10, 10, 20, 20)
		 
		        Using fs As New FileStream(strFileName, FileMode.Create)
		            PdfWriter.GetInstance(document, fs)
		                'Dim parsedList As System.Collections.Generic.List(Of IElement) = iTextSharp.text.html.simpleparser.HTMLWorker.ParseToList(stringReader, Nothing)
		                document.Open()
				       	
				       	Dim table As PdfPTable = New PdfPTable(2)
				       	table.WidthPercentage = 100
				       	''
				       	Dim eprCell As PdfPCell = New PdfPCell(New Phrase("Patient Registration"))
						eprCell.Colspan = 2
						eprCell.HorizontalAlignment = 1
						table.AddCell(eprCell)
						''
				       	Dim neweCell As PdfPCell = New PdfPCell(New Phrase("Location"))
						neweCell.Colspan = 2
						neweCell.HorizontalAlignment = 1
						table.AddCell(neweCell)
						''
						table.AddCell("Please indicate your preferred clinic")
						table.AddCell(prefClinic)
						''
				       	Dim evprCell As PdfPCell = New PdfPCell(New Phrase("New Patient Registration Form"))
						evprCell.Colspan = 2
						evprCell.HorizontalAlignment = 1
						table.AddCell(evprCell)
						''
						table.AddCell("Patient Name")
						table.AddCell(newPatient.FirstName & " " & newPatient.LastName)
						''
						table.AddCell("Address")
						table.AddCell(newPatient.Address1 & " " & newPatient.City & ", " & newPatient.State & " " & newPatient.Zip)
						''
						table.AddCell("Date Of Birth")
						table.AddCell(newPatient.DateOfBirth)
						''
						table.AddCell("Gender")
						table.AddCell(newPatient.Gender)
						''
						table.AddCell("Email Address")
						table.AddCell(newPatient.EmailAddress)
						''
						table.AddCell("Phone")
						table.AddCell(newPatient.Phone)
						''
						table.AddCell("If you wish to receive any information or promotions from Express Weight Loss Clinic:")
						table.AddCell(CleanPDFString(newPatient.Promos))
						''
				       	Dim medHistCell As PdfPCell = New PdfPCell(New Phrase("Medical History"))
						medHistCell.Colspan = 2
						medHistCell.HorizontalAlignment = 1
						table.AddCell(medHistCell)
						''

						''



						Dim ThisImage = iTextSharp.text.Image.GetInstance(SampleImage)
						ThisImage.Alignment = iTextSharp.text.Image.ALIGN_CENTER

						table.AddCell("Current/Past Medical History. Check all that apply:")
						table.AddCell(CleanPDFString(newPatientHistory.curMedHistory))
						''
						table.AddCell("Past Surgical History. Check all that apply:")
						table.AddCell(CleanPDFString(newPatientHistory.surgHistory))
						''
						table.AddCell("Social History. Check all that apply:")
						table.AddCell(CleanPDFString(newPatientHistory.socialHistory))
						''
						table.AddCell("Review of Symptoms. Check all that apply:")
						table.AddCell(CleanPDFString(newPatientHistory.revSymptoms))
						''
						table.AddCell("Eating Habits. Check all that apply:")
						table.AddCell(CleanPDFString(newPatientHistory.eatHabits))
						''
						table.AddCell("Physical Activity. Check if applicable:")
						table.AddCell(CleanPDFString(newPatientHistory.physActivity))
						''
						table.AddCell("Allergies to Medications. Please List:")
						table.AddCell(CleanPDFString(newPatientHistory.allergiesToMeds))
						''
						table.AddCell("Current Medications:")
						table.AddCell(CleanPDFString(newPatientHistory.curMeds))
						''
				       	Dim patientWaiverCell As PdfPCell = New PdfPCell(New Phrase("PATIENT WAIVER AND INFORMATIONAL CONSENT FORM"))
						patientWaiverCell.Colspan = 2
						patientWaiverCell.HorizontalAlignment = 1
						table.AddCell(patientWaiverCell)
						''
				       	Dim noticeOfPrivacy As PdfPCell = New PdfPCell(New Phrase("Notice of Privacy Practices Form"))
						noticeOfPrivacy.Colspan = 2
						noticeOfPrivacy.HorizontalAlignment = 1
						table.AddCell(noticeOfPrivacy)
						''
				       	Dim blank As PdfPCell = New PdfPCell(New Phrase(""))
						blank.Colspan = 2
						blank.HorizontalAlignment = 1
						table.AddCell(blank)
						''
						table.AddCell("Acknowledgement")
						table.AddCell("By signing below, I certify that the information provided for the New Patient Registration form is true and accurate to the best of my knowledge. I acknowledge that I have thoroughly read the Patient Waiver and Information Consent forms, agree to the terms and consent to treatment. I understand that Express Weight Loss Clinic will provide me a copy of these waivers and forms upon my request. I acknowledge that I have received a copy of these waivers and forms, or that I have declined to receive copies.")
						'Image 	Cell
				       	Dim imageCell As PdfPCell = New PdfPCell(ThisImage, True)
				       	imageCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER
						table.AddCell("Signature")
						table.AddCell(imageCell)
						''
						table.AddCell("Printed Name")
						table.AddCell(newPatientHistory.printedFirstName & " " & newPatientHistory.printedLastName)
						''
						table.AddCell("Todays Date")
						table.AddCell(GetDatePDF())
						''
						table.AddCell("I am 18 years of age or older")
						table.AddCell(newPatientHistory.above18)


						''
				       	Dim endThanks As PdfPCell = New PdfPCell(New Phrase("Thank you for completing the E-Visit Patient Registration. Click submit below to be routed to our scheduling page. You have to schedule using the calendar to be be seen via E-Visit"))
						endThanks.Colspan = 2
						endThanks.HorizontalAlignment = 1
						table.AddCell(endThanks)
						

						document.Add(table)

	                    

		                'document.NewPage()


	                    'document.Add(ThisImage)

		                ' parse each html object and add it to the pdf document
		                'For Each item As Object In parsedList
		                    'document.Add(DirectCast(item, IElement))
		                'Next

		 
		                document.Close()
		 
		 
		        End Using
	        
		Catch ex As Exception
			WriteToSpecifiedLog(ex.ToString, dir)
		End Try




End Function









Public Function UppercaseFirstLetter(ByVal val As String) As String
    ' Test for nothing or empty.
    If String.IsNullOrEmpty(val) Then
        Return val
    End If

    ' Convert to character array.
    Dim array() As Char = val.ToCharArray

    ' Uppercase first character.
    array(0) = Char.ToUpper(array(0))

    ' Return new string.
    Return New String(array)
End Function




Public Function CleanString(strClean As String) As String



	strClean = strClean.Replace("\r", " ")
	strClean = strClean.Replace(vbCrLf, "")
	strClean = strClean.Replace("[", "")
	strClean = strClean.Replace("]", "")
	strClean = strClean.Replace(Chr(34), "")

	Return strClean

End Function


Public Function SaveSignatureFile(url As String, filename As String) As String

	Try
		Dim nfilename As String = "C:\Users\Public\Scanned Charts\" & filename & ".png"
		Dim filenamePdf As String = "C:\Users\Public\Scanned Charts\" & filename & ".pdf"
		If Not File.Exists(nfilename) Then
			File.Delete(nfilename)
			My.Computer.Network.DownloadFile(url, nfilename)
		End If
		

		If File.Exists(nfilename) Then
			'Convert File to PDF 


		    Dim doc As New PdfSharp.Pdf.PdfDocument()

	        Try
	            Dim source As String = nfilename
	            Dim oPage As New PdfSharp.Pdf.PDFPage()

	            doc.Pages.Add(oPage)
	            Dim xgr As XGraphics = XGraphics.FromPdfPage(oPage)
	            Dim img As XImage = XImage.FromFile(source)

	            xgr.DrawImage(img, 0, 0)
	            'success = True
	        Catch ex As Exception

				SendErrorEmail(ex.ToString())
	            'MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
	        End Try
		    

		    'Dim destinaton As String = (TryCast(e.Argument, String()))(1)
		    doc.Save(filenamePdf)
		    doc.Close()

			'My.Computer.FileSystem.DeleteFile(nfilename)







		End If

		Return "Download Success"
	Catch ex As Exception
		Return ex.ToString()
	End Try


End Function


Public Function SendErrorEmail(msg As String) As String


	Try

		Dim SmtpServer As New SmtpClient()
		Dim mail As New MailMessage()
		SmtpServer.Credentials = New _
		System.Net.NetworkCredential("errors.techknowvation@gmail.com", "p@ssw0rd4me!")
		SmtpServer.Port = 587
		SmtpServer.Host = "smtp.gmail.com"
		SmtpServer.EnableSsl = True
		mail = New MailMessage()
		mail.From = New MailAddress("noreply@techknowvation.com")
		mail.To.Add("austinrsellers@gmail.com")
		mail.CC.Add("steve.sellers@techknowvation.com")
		mail.Subject = "Error"
		mail.Body = msg
		SmtpServer.Send(mail)

	Catch ex As Exception
		WriteToLog(ex.ToString())
	End Try
            

End Function


Public Function FormatProperCase(str As String) As String

	Dim strRet As String
	Try
		strRet = StrConv(str, VbStrConv.ProperCase)
	Catch ex As Exception
		SendErrorEmail(ex.ToString())
	End Try

	Return strRet

End Function

Public Function ConvertDate(newDate As String) As String


	If Not newDate = "" Then 
		Dim myDateString as String = newDate
		Dim myDate as DateTime
		DateTime.TryParse(myDateString, myDate).ToString()

		Return myDate.ToString("MM/yyyy")
	Else
		Return ""
	End If
	

End Function

Public Function CheckForNull(curMeds As String) As String

	If Trim(curMeds.ToLower()) = "none" Or Trim(curMeds.ToLower()) = "na" Or Trim(curMeds.ToLower()) = "n/a" Then
		Return ""
	Else
		Return curMeds
	End If


End Function



<WebMethod()> _
Public Sub testCompilation()


        Try
            'Dim SmtpServer As New SmtpClient()
            'Dim mail As New MailMessage()
            'SmtpServer.Credentials = New _
			'Net.NetworkCredential("errors.techknowvation@gmail.com", "p@ssw0rd4me!")
            'SmtpServer.Port = 587
            'SmtpServer.Host = "smtp.gmail.com"
            'SmtpServer.EnableSsl = True
            'mail = New MailMessage()
            'mail.From = New MailAddress("noreply@techknowvation.com")
            'mail.To.Add("austinrsellers@gmail.com")
            'mail.Subject = "Test Mail"
            'mail.Body = "This is for testing SMTP mail from GMAIL"
            'SmtpServer.Send(mail)
        Catch ex As Exception
        	WriteToLog(ex.ToString())
			SendErrorEmail(ex.ToString())
        End Try






	HttpContext.Current.Response.BufferOutput = True
	HttpContext.Current.Response.ContentType = "application/x-javascript"
	HttpContext.Current.Response.Write("Success")
	HttpContext.Current.Response.Flush()


End Sub




<WebMethod()> _
Public Sub testWriteToNet()

	Dim retString As String
        Try
        	'Dim testDir As String() = Directory.GetFiles("\\192.168.1.124\ScannedCharts\Signed Consent Forms\")
        	'WriteToSpecifiedLog(testDir(0), WriteTestLog)
	        Dim FILE_NAME As String = "\\192.168.1.124\ScannedCharts\Signed Consent Forms\test2.txt"

			If System.IO.File.Exists(FILE_NAME) = True Then

				Dim objWriter As New System.IO.StreamWriter( FILE_NAME )

				objWriter.Write( "This is a Test" )
				objWriter.Close()

			Else

		      Using sw As StreamWriter = File.CreateText(FILE_NAME)
		        sw.WriteLine("Hello")
		        sw.WriteLine("And")
		        sw.WriteLine("Welcome")
		      End Using

			End If
            'Dim SmtpServer As New SmtpClient()
            'Dim mail As New MailMessage()
            'SmtpServer.Credentials = New _
			'Net.NetworkCredential("errors.techknowvation@gmail.com", "p@ssw0rd4me!")
            'SmtpServer.Port = 587
            'SmtpServer.Host = "smtp.gmail.com"
            'SmtpServer.EnableSsl = True
            'mail = New MailMessage()
            'mail.From = New MailAddress("noreply@techknowvation.com")
            'mail.To.Add("austinrsellers@gmail.com")
            'mail.Subject = "Test Mail"
            'mail.Body = "This is for testing SMTP mail from GMAIL"
            'SmtpServer.Send(mail)
            retString += "Success"

        	WriteToSpecifiedLog("Success",WriteTestLog)
        Catch ex As Exception
            retString += ex.ToString()
        	WriteToSpecifiedLog(ex.ToString(),WriteTestLog)
			'SendErrorEmail(ex.ToString())
        End Try






	HttpContext.Current.Response.BufferOutput = True
	HttpContext.Current.Response.ContentType = "application/x-javascript"
	HttpContext.Current.Response.Write(retString)
	HttpContext.Current.Response.Flush()


End Sub






    

    
<WebMethod()> _
Public Sub EVisitPatient()

	WriteToSpecifiedLog(GetDateTime() & " : ",EVisitPatientLog)
	Dim retLog As String


	Dim buffer As Byte() = New Byte(context.Request.InputStream.Length - 1) {}
	context.Request.InputStream.Read(buffer, 0, buffer.Length)
	context.Request.InputStream.Position = 0




    Dim jData As String
	Using reader As New StreamReader(HttpContext.Current.Request.InputStream)
		jData += reader.ReadToEnd()
	End Using

	WriteToSpecifiedLog(vbCrLf & jData,EVisitPatientLog)
	If jData.Contains("null") Then
		jData = jData.Replace("null", Chr(34) & Chr(34))
	End If

			
	Dim x As Object = JsonConvert.DeserializeObject(of Object)(jData)



	Try

		If Not x Is Nothing Then
			Dim FormID = x("FormID")
			Dim UniqueID = x("UniqueID") 

			Dim firstVisit
			If jData.Contains("Is this your first E-Visit with Express Weight Loss Clinic?") Then
				firstVisit = x("Is this your first E-Visit with Express Weight Loss Clinic?").ToString()
			End If

				
			Dim firstName = x("Patient Name")("first")
			Dim lastName = x("Patient Name")("last")
			Dim Address1 = x("Address")("address")

			Dim Address2
			If jData.Contains("address2") Then
				Address2 = x("Address")("address2").ToString()
			End If

			Dim City = x("Address")("city")
			Dim State = x("Address")("state")
			Dim Zip = x("Address")("zip")
			Dim dob = x("Date of Birth")
			Dim Gender = x("Gender")
			Dim EmailAddress = x("Email Address")
			Dim Phone = x("Phone")
			
			Dim Promos
			If jData.Contains("If you wish to receive any information or promotions from Express Weight Loss Clinic:") Then
				Promos = x("If you wish to receive any information or promotions from Express Weight Loss Clinic:").ToString()
			End If
			
		
WriteToSpecifiedLog("test", EVisitPatientLog)	

			Dim curMedHistory = x("Current/Past Medical History.  Check all that apply:").ToString()
			Dim heartAttack = x("Heart Attack - Date of Last Occurrence")
			Dim strokeDate = x("Stroke - Date of Last Occurrence")
			Dim medOther = x("Medical History Other (Please explain):")
			Dim other = x("Past Surgery Other (Please explain):")
			Dim surgHistory = x("Past Surgical History.  Check all that apply:").ToString()
			Dim socialHistory = x("Social History.  Check all that apply:").ToString()
			Dim alcoholConsumption = x("Alcohol Consumption")
			Dim illicitDrugs = x("Illicit Drugs:")
			Dim revSymptoms = x("Review of Symptoms.  Check all that apply:").ToString()
			Dim eatHabits = x("Eating Habits.  Check all that apply:").ToString()
			Dim physActivity = x("Physical Activity.  Check if applicable:")
			Dim allergiesToMeds = x("Allergies to Medications.  Please List:").ToString()
			Dim curMeds = x("Current Medications:").ToString()
			Dim Waiver = x("Acknowledgement")
			Dim Signature = x("Signature")


			Dim printedFirstName = ""
			Dim printedLastName = ""

			If Not x("Patient Printed Name").ToString() = "" Then

				printedFirstName = x("Patient Printed Name")("first")
				printedLastName = x("Patient Printed Name")("last")

			End If
			

			Dim todaysDate = x("Today's Date")
			Dim above18 = x("I am 18 years of age or older")
			Dim Consent = x("Consent to treat minor children")
			Dim gSig = x("Guardian's Signature")
			Dim pDob = x("Patient Date of Birth")
			Dim gPrintedName = x("Guardian's Printed Name")
			Dim haveID = x("Do you have your government issued driver's license or ID?")
	   		Dim formOfPayment = x("Do you have your form of payment?")
	   		Dim haveScale = x("Do you have your scale?")
	   		Dim autoBloodPressure = x("Do you have your automatic (electronic) blood pressure cuff?")
	   		Dim measuringTape = x("Do you have your measuring tape?")
	   		Dim haveWebcam = x("Do you have a web cam or video enabled smart device?")
	   		Dim whichDevice = x("Which device will you be using for the E-Visit")

	   		Dim newEvisitDetails As New EVisitDetails

	   		Try 

				With newEvisitDetails

			        '.todaysDate = todaysDate
			        .above18 = above18
			        .Consent = Consent
			        .gSig = gSig
			        .pDob = pDob
			        .gPrintedName = gPrintedName
			        .haveID = haveID
			        .formOfPayment = formOfPayment
			        .haveScale = haveScale
			        .autoBloodPressure = autoBloodPressure
			        .measuringTape = measuringTape
			        .haveWebcam = haveWebcam
			        .whichDevice = whichDevice

			    End With

	   		Catch ex As Exception
	   			WriteToSpecifiedLog(ex.ToString(), EVisitPatientLog)
	   		End Try





			Dim newPatient As New Patient
			Dim newPatientHistory As New PatientHistory

			With newPatient

				.FirstName = Trim(FormatProperCase(firstName))
		        .LastName = Trim(FormatProperCase(lastName))
		        .Address1 = FormatProperCase(Address1)
		        .Address2 = FormatProperCase(Address2)
		        .City = FormatProperCase(City)
		        .State = State
		        .Zip = Zip
		        .DateOfBirth = dob
		        .Gender = Gender
		        .EmailAddress = EmailAddress
		        .Phone = Phone
		        .Promos = Promos

		    End With

			newPatient.MedicalRecordNum = GenerateID(newPatient)

			With newPatient
			
					retLog += vbCrLf & .FirstName
		            retLog += vbCrLf & .LastName
		            retLog += vbCrLf & .Address1
		            retLog += vbCrLf & .Address2
		            retLog += vbCrLf & .City
		            retLog += vbCrLf & .State
		            retLog += vbCrLf & .Zip
		            retLog += vbCrLf & .DateOfBirth
		            retLog += vbCrLf & .Gender
		            retLog += vbCrLf & .EmailAddress
		            retLog += vbCrLf & .Phone
		            retLog += vbCrLf & .Promos
		            retLog += vbCrLf & .MedicalRecordNum

		    End With

		    Dim strCurMedHistory As String = curMedHistory
		    Dim strSurgHistory As String = surgHistory
		    Dim strAllergiesToMeds As String = allergiesToMeds
		    Dim strIllicitDrugs As String = illicitDrugs
		    'Build Array - "\r"



		    Dim strCurMeds As String = curMeds



		    strCurMedHistory = CleanString(strCurMedHistory)
		    strSurgHistory = CleanString(strSurgHistory)
		    strAllergiesToMeds = CleanString(strAllergiesToMeds)
		    strIllicitDrugs = CleanString(strIllicitDrugs)
		    strCurMeds = CleanString(strCurMeds)


		    If strCurMedHistory.Length() > 250 Then
		    	strCurMedHistory = strCurMedHistory.SubString(0, 248)
		    	strCurMedHistory = strCurMedHistory.Replace(vbCrLf, "")
		    End If

		    If strSurgHistory.Length() > 250 Then
		    	strSurgHistory = strSurgHistory.SubString(0, 248)
		    	strSurgHistory = strSurgHistory.Replace(vbCrLf, "")
		    End If

		    If strAllergiesToMeds.Length() > 250 Then
		    	strAllergiesToMeds = strAllergiesToMeds.SubString(0, 248)
		    	strAllergiesToMeds = strAllergiesToMeds.Replace(vbCrLf, "")
		    End If

		    If strIllicitDrugs.Length() > 100 Then
		    	strIllicitDrugs = strIllicitDrugs.SubString(0, 98)
		    	strIllicitDrugs = strIllicitDrugs.Replace(vbCrLf, "")
		    End If

		    If strCurMeds.Length() > 100 Then
		    	strCurMeds = strCurMeds.SubString(0, 98)
		    	strCurMeds = strCurMeds.Replace(vbCrLf, "")
		    End If




		    With newPatientHistory


				'.medHistory = medHistory
				.curMedHistory = strCurMedHistory
				.heartAttack = heartAttack
				.strokeDate = strokeDate
				.other = other
				.medOther = medOther
				.surgHistory = strSurgHistory
				.socialHistory = socialHistory
				.alcoholConsumption = alcoholConsumption
				.illicitDrugs = strIllicitDrugs
				.revSymptoms = revSymptoms
				.eatHabits = eatHabits
				.physActivity = physActivity
				.allergiesToMeds = UppercaseFirstLetter(strAllergiesToMeds.ToLower())
				.curMeds = CheckForNull(curMeds)
				.Waiver = Waiver
				'.Notice = Notice
				'.Acknowledgement = Acknowledgement
				.Signature = Signature
				.printedFirstName = Trim(printedFirstName)
				.printedLastName = Trim(printedLastName)
				.todaysDate = todaysDate
				.above18 = above18
				.Consent = Consent
				.gSig = gSig
				.pDob = pDob
				.gPrintedName = gPrintedName
				'.Thanks = Thanks



		    End With


		    With newPatientHistory


				retLog += vbCrLf & .medHistory
				retLog += vbCrLf & .curMedHistory
				retLog += vbCrLf & .heartAttack
				retLog += vbCrLf & .strokeDate
				retLog += vbCrLf & .other
				retLog += vbCrLf & .medOther
				retLog += vbCrLf & .surgHistory
				retLog += vbCrLf & .socialHistory
				retLog += vbCrLf & .alcoholConsumption
				retLog += vbCrLf & .illicitDrugs
				retLog += vbCrLf & .revSymptoms
				retLog += vbCrLf & .eatHabits
				retLog += vbCrLf & .physActivity
				retLog += vbCrLf & .allergiesToMeds
				retLog += vbCrLf & .curMeds
				retLog += vbCrLf & .Waiver
				retLog += vbCrLf & .Notice
				retLog += vbCrLf & .Acknowledgement
				retLog += vbCrLf & .Signature
				retLog += vbCrLf & .printedFirstName
				retLog += vbCrLf & .printedLastName
				retLog += vbCrLf & .todaysDate
				retLog += vbCrLf & .above18
				retLog += vbCrLf & .Consent
				retLog += vbCrLf & .gSig
				retLog += vbCrLf & .pDob
				retLog += vbCrLf & .gPrintedName
				'retLog += vbCrLf & .Thanks



		    End With



		    WriteToSpecifiedLog(retLog, EVisitPatientLog)




			WriteToSpecifiedLog("Medical Record Number - " & newPatient.MedicalRecordNum, EVisitPatientLog)


			If firstVisit.ToString().ToLower() = "no" Then
				retLog += WritePatientDataEVisitNo(newPatient, ProdServer, EVisitPatientLog)



			Else
				retLog += WritePatientDataEVisitYes(newPatient, newPatientHistory, newPatientHistory.curMeds, ProdServer, EVisitPatientLog)




			End If


			Dim htmlContent As String = ""


	    	
			Dim sFilename As String = newPatient.MedicalRecordNum & " - " & newPatient.FirstName & " " & newPatient.LastName & " - " & "Evisit" 
	 		'Dim fileSaveResult As String = SaveSignatureFileGlobal(Signature, "C:\Users\Public\Scanned Charts - EVisitPatients\", sFilename)



	 		'htmlContent = FormHtmlContent(newPatient, newPatientHistory, "C:\Users\Public\Scanned Charts - EVisitPatients\", sFilename, EVisitPatientLog)

	 		Dim fileSaveResult2 As String = WriteHtmlContent2(Signature, firstVisit, newPatient, newPatientHistory, newEvisitDetails, saveDirectory, sFilename, EVisitPatientLog)




	 		If File.Exists(saveDirectory & sFilename & ".png") Then
	 			File.Delete(saveDirectory & sFilename & ".png")
	 		End If
	    	'retLog += WritePatientHistoryDataGlobal(newPatientHistory, newPatient.MedicalRecordNum)


	    	'If Not CheckForNull(strCurMeds) = "" Then

	    		'WriteMedicationListUpdate(newPatient.MedicalRecordNum, strCurMeds)

	    	'End If

		
			'Dim sFilename As String = newPatient.MedicalRecordNum & " - " & newPatient.FirstName & " " & newPatient.LastName
		 	'Dim fileSaveResult As String = SaveSignatureFile(Signature, sFilename)
		 	'WriteToLog("File Write Log: " & fileSaveResult)




	 	End If







	Catch ex As Exception
		WriteToSpecifiedLog(ex.ToString(), EVisitPatientLog)
	End Try





	HttpContext.Current.Response.BufferOutput = True
	HttpContext.Current.Response.ContentType = "application/x-javascript"
	HttpContext.Current.Response.Write("Success EVisitPatient")
	HttpContext.Current.Response.Flush()


End Sub



Public Function FormHtmlContent(newPatient As Patient, newPatientHistory As PatientHistory, fileDir As String, filename As String, dir As String) As String
	Dim content As String	        
	Dim strFileShortName As String = filename & ".pdf"
    Dim strPngShortName As String = filename & ".png"
    Dim strFileName As String = fileDir & strFileShortName



	content += "<table style='border:1px solid #000;'>"


	content += "<tr style='border:1px solid #000;'><td style='border:1px solid #000;'>Testttttttt</td><td>Testttt</td></tr><tr><td>Image</td><td><img style='width:100%;height:auto;' src=""" & fileDir & strPngShortName & """ /></td></tr>"


	content += "</table>"

	Return content

End Function


'Starts Global Functionality

Public Function WriteHtmlContent(htmlContent As String, url As String, newPatient As Patient, newPatientHistory As PatientHistory, fileDir As String, filename As String, dir As String) As String

		Try 






			Dim nfilename As String = fileDir & filename & ".png"
			Dim filenamePdf As String = fileDir & filename & ".pdf"
			If Not File.Exists(nfilename) Then
				File.Delete(nfilename)
				My.Computer.Network.DownloadFile(url, nfilename)
			Else
				File.Delete(nfilename)
				My.Computer.Network.DownloadFile(url, nfilename)
			End If



	        Dim strFileShortName As String = filename & ".pdf"
	        Dim strPngShortName As String = filename & ".png"
	        Dim strFileName As String = fileDir & strFileShortName









        	Dim SampleImage = Path.Combine(fileDir, strPngShortName)





			' Read html file to a string
			Dim line As String

			line = htmlContent

			' Code to convert to pdf
			Dim fsNew As New StringReader(line)
			Dim document As New Document(iTextSharp.text.PageSize.LETTER, 10, 10, 20, 20)
	 
	        Using fs As New FileStream(strFileName, FileMode.Create)
	            PdfWriter.GetInstance(document, fs)
	            Using stringReader As New StringReader(line)
	                Dim parsedList As System.Collections.Generic.List(Of IElement) = iTextSharp.text.html.simpleparser.HTMLWorker.ParseToList(stringReader, Nothing)
	                document.Open()


			       	Dim table As PdfPTable = New PdfPTable(2)
			       	table.WidthPercentage = 100
			       	Dim cell As PdfPCell = New PdfPCell(New Phrase("Form Data"))
					cell.Colspan = 2
					cell.HorizontalAlignment = 1
					table.AddCell(cell)
					table.AddCell("Col 1 Row 1")
					table.AddCell("Col 2 Row 1")
					table.AddCell("Col 3 Row 1")
					table.AddCell("Col 1 Row 2")
					table.AddCell("Col 2 Row 2")
					table.AddCell("Col 3 Row 2")

			        'document.Add(table)

                    Dim ThisImage = iTextSharp.text.Image.GetInstance(SampleImage)

	                document.NewPage()


                    'document.Add(ThisImage)

	                ' parse each html object and add it to the pdf document
	                For Each item As Object In parsedList
	                    document.Add(DirectCast(item, IElement))
	                Next

	 
	                document.Close()
	 
	            End Using
	 
	        End Using
		Catch ex As Exception
			WriteToSpecifiedLog(ex.ToString, dir)
		End Try




End Function





Public Function WriteHtmlContent2(url As String, firstVisit As String, newPatient As Patient, newPatientHistory As PatientHistory, newEvisitDetails As EVisitDetails, fileDir As String, filename As String, dir As String) As String

		Try 


			Dim nfilename As String = fileDir & filename & ".png"
			Dim filenamePdf As String = fileDir & filename & ".pdf"


			If Not url = "" Then


				If Not File.Exists(nfilename) Then
					File.Delete(nfilename)
					My.Computer.Network.DownloadFile(url, nfilename)
				Else
					File.Delete(nfilename)
					My.Computer.Network.DownloadFile(url, nfilename)
				End If


			End If

		        Dim strFileShortName As String = filename & ".pdf"
		        Dim strPngShortName As String = filename & ".png"
		        Dim strFileName As String = fileDir & strFileShortName



			



	        	Dim SampleImage = Path.Combine(fileDir, strPngShortName)






				Dim document As New Document(iTextSharp.text.PageSize.LETTER, 10, 10, 20, 20)
		 
		        Using fs As New FileStream(strFileName, FileMode.Create)
		            PdfWriter.GetInstance(document, fs)
		                'Dim parsedList As System.Collections.Generic.List(Of IElement) = iTextSharp.text.html.simpleparser.HTMLWorker.ParseToList(stringReader, Nothing)
		                document.Open()
				       	
				       	Dim table As PdfPTable = New PdfPTable(2)
				       	table.WidthPercentage = 100
				       	''
				       	Dim eprCell As PdfPCell = New PdfPCell(New Phrase("E-Patient Registration"))
						eprCell.Colspan = 2
						eprCell.HorizontalAlignment = 1
						table.AddCell(eprCell)
						''
				       	Dim neweCell As PdfPCell = New PdfPCell(New Phrase("New E-Visit Patient Registration"))
						neweCell.Colspan = 2
						neweCell.HorizontalAlignment = 1
						table.AddCell(neweCell)
						''
						
						table.AddCell("Is this your first E-Visit with Express Weight Loss Clinic?")
						table.AddCell(firstVisit)
						''
				       	Dim evprCell As PdfPCell = New PdfPCell(New Phrase("E-Visit Patient Registration"))
						evprCell.Colspan = 2
						evprCell.HorizontalAlignment = 1
						table.AddCell(evprCell)
						''
						table.AddCell("Patient Name")
						table.AddCell(newPatient.FirstName & " " & newPatient.LastName)
						''
						table.AddCell("Address")
						table.AddCell(newPatient.Address1 & " " & newPatient.City & ", " & newPatient.State & " " & newPatient.Zip)
						''
						table.AddCell("Date Of Birth")
						table.AddCell(newPatient.DateOfBirth)
						''
						table.AddCell("Gender")
						table.AddCell(newPatient.Gender)
						''
						table.AddCell("Email Address")
						table.AddCell(newPatient.EmailAddress)
						''
						table.AddCell("Phone")
						table.AddCell(newPatient.Phone)
						''
						table.AddCell("If you wish to receive any information or promotions from Express Weight Loss Clinic:")
						table.AddCell(CleanPDFString(newPatient.Promos))
						''
				       	Dim medHistCell As PdfPCell = New PdfPCell(New Phrase("Medical History"))
						medHistCell.Colspan = 2
						medHistCell.HorizontalAlignment = 1
						table.AddCell(medHistCell)
						''

						''
						If firstVisit.ToLower() = "yes" Then



							Dim ThisImage = iTextSharp.text.Image.GetInstance(SampleImage)
							ThisImage.Alignment = iTextSharp.text.Image.ALIGN_CENTER

							table.AddCell("Current/Past Medical History. Check all that apply:")
							table.AddCell(CleanPDFString(newPatientHistory.curMedHistory))
							''
							table.AddCell("Past Surgical History. Check all that apply:")
							table.AddCell(CleanPDFString(newPatientHistory.surgHistory))
							''
							table.AddCell("Social History. Check all that apply:")
							table.AddCell(CleanPDFString(newPatientHistory.socialHistory))
							''
							table.AddCell("Review of Symptoms. Check all that apply:")
							table.AddCell(CleanPDFString(newPatientHistory.revSymptoms))
							''
							table.AddCell("Eating Habits. Check all that apply:")
							table.AddCell(CleanPDFString(newPatientHistory.eatHabits))
							''
							table.AddCell("Physical Activity. Check if applicable:")
							table.AddCell(CleanPDFString(newPatientHistory.physActivity))
							''
							table.AddCell("Allergies to Medications. Please List:")
							table.AddCell(CleanPDFString(newPatientHistory.allergiesToMeds))
							''
							table.AddCell("Current Medications:")
							table.AddCell(CleanPDFString(newPatientHistory.curMeds))
							''
					       	Dim patientWaiverCell As PdfPCell = New PdfPCell(New Phrase("PATIENT WAIVER AND INFORMATIONAL CONSENT FORM"))
							patientWaiverCell.Colspan = 2
							patientWaiverCell.HorizontalAlignment = 1
							table.AddCell(patientWaiverCell)
							''
					       	Dim noticeOfPrivacy As PdfPCell = New PdfPCell(New Phrase("Notice of Privacy Practices Form"))
							noticeOfPrivacy.Colspan = 2
							noticeOfPrivacy.HorizontalAlignment = 1
							table.AddCell(noticeOfPrivacy)
							''
					       	Dim blank As PdfPCell = New PdfPCell(New Phrase(""))
							blank.Colspan = 2
							blank.HorizontalAlignment = 1
							table.AddCell(blank)
							''
							table.AddCell("Acknowledgement")
							table.AddCell("By signing below, I certify that the information provided for the New Patient Registration form is true and accurate to the best of my knowledge. I acknowledge that I have thoroughly read the Patient Waiver and Information Consent forms, agree to the terms and consent to treatment. I understand that Express Weight Loss Clinic will provide me a copy of these waivers and forms upon my request. I acknowledge that I have received a copy of these waivers and forms, or that I have declined to receive copies.")
							'Image 	Cell
					       	Dim imageCell As PdfPCell = New PdfPCell(ThisImage, True)
					       	imageCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER
							table.AddCell("Signature")
							table.AddCell(imageCell)
							''
							table.AddCell("Printed Name")
							table.AddCell(newPatientHistory.printedFirstName & " " & newPatientHistory.printedLastName)
							''
							table.AddCell("Todays Date")
							table.AddCell(GetDatePDF())




						End If 
						''
				       	Dim prepEvisit As PdfPCell = New PdfPCell(New Phrase("Prepare for E-Visit Checklist"))
						prepEvisit.Colspan = 2
						prepEvisit.HorizontalAlignment = 1
						table.AddCell(prepEvisit)
						''
						table.AddCell("Do you have your government issued driver's license or ID?")
						table.AddCell(newEvisitDetails.haveID)
						''
						table.AddCell("Do you have your form of payment?")
						table.AddCell(newEvisitDetails.formOfPayment)
						''
						table.AddCell("Do you have your scale?")
						table.AddCell(newEvisitDetails.haveScale)
						''
						table.AddCell("Do you have your automatic (electronic) blood pressure cuff?")
						table.AddCell(newEvisitDetails.autoBloodPressure)
						''
						table.AddCell("Do you have your measuring tape?")
						table.AddCell(newEvisitDetails.measuringTape)
						''
						table.AddCell("Do you have a web cam or video enabled smart device?")
						table.AddCell(newEvisitDetails.haveWebcam)
						''
						table.AddCell("Which device will you be using for the E-Visit")
						table.AddCell(newEvisitDetails.whichDevice)
						''
				       	Dim endThanks As PdfPCell = New PdfPCell(New Phrase("Thank you for completing the E-Visit Patient Registration. Click submit below to be routed to our scheduling page. You have to schedule using the calendar to be be seen via E-Visit"))
						endThanks.Colspan = 2
						endThanks.HorizontalAlignment = 1
						table.AddCell(endThanks)
						

						document.Add(table)

	                    

		                'document.NewPage()


	                    'document.Add(ThisImage)

		                ' parse each html object and add it to the pdf document
		                'For Each item As Object In parsedList
		                    'document.Add(DirectCast(item, IElement))
		                'Next

		 
		                document.Close()
		 
		 
		        End Using
	        
		Catch ex As Exception
			WriteToSpecifiedLog(ex.ToString, dir)
		End Try




End Function



Public Function CleanPDFString(strClean As String) As String


	If Not strClean = "" Then
		strClean = strClean.Replace("\r", " ")
		strClean = strClean.Replace("\n", "")
		'strClean = strClean.Replace(vbCrLf, " ")
		strClean = strClean.Replace("[", "")
		strClean = strClean.Replace("]", "")
		strClean = strClean.Replace(Chr(34), "")
	End If



	Return Trim(strClean)

End Function



Public Function GetDatePDF() As String

	Dim myDateString as String = Date.Today.ToString()
	Dim myDate as DateTime
	DateTime.TryParse(myDateString, myDate).ToString()

	Return myDate.ToString("MMM dd, yyyy")

End Function



Public Function SaveSignatureFileGlobal(url As String, fileDir As String, filename As String) As String

	Try
		Dim nfilename As String = fileDir & filename & ".png"
		Dim filenamePdf As String = fileDir & filename & ".pdf"
		If Not File.Exists(nfilename) Then
			File.Delete(nfilename)
			My.Computer.Network.DownloadFile(url, nfilename)
		End If
		

		If File.Exists(nfilename) Then
			'Convert File to PDF 


		    Dim doc As New PdfSharp.Pdf.PdfDocument()

	        Try
	            Dim source As String = nfilename
	            Dim oPage As New PdfSharp.Pdf.PDFPage()

	            doc.Pages.Add(oPage)
	            Dim xgr As XGraphics = XGraphics.FromPdfPage(oPage)
	            Dim img As XImage = XImage.FromFile(source)

	            xgr.DrawImage(img, 0, 0)
	            'success = True
	        Catch ex As Exception

				SendErrorEmail(ex.ToString())
	            'MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
	        End Try
		    

		    'Dim destinaton As String = (TryCast(e.Argument, String()))(1)
		    doc.Save(filenamePdf)
		    doc.Close()

			'My.Computer.FileSystem.DeleteFile(nfilename)







		End If

		Return "Download Success"
	Catch ex As Exception
		Return ex.ToString()
	End Try


End Function


Public Function WritePatientDataEVisitNo(ByVal newPatient As Patient, sConnString As String, dir As String) As String


	WriteToSpecifiedLog("Patient: No", dir)


	Dim promotionsEmail As String = "0"
	Dim promotionsText As String = "0"
	Dim promotionsMail As String = "0"
	Dim promotionsNoThankYou As String = "0"

	Dim newPhone As String = newPatient.Phone.Replace("(","")
	newPhone = newPhone.Replace(")","")
	newPhone = newPhone.Replace(" ","-")
	newPhone = newPhone.Replace(vbCrLf,"")

	With newPatient

		If  .Promos.Contains("Email") Then
			promotionsEmail = "1"
		End If
		If  .Promos.Contains("Text") Then
			promotionsText = "1"
		End If
		If  .Promos.Contains("Mail") Then
			promotionsMail = "1"
		End If

	End With

	Dim retString As String

	Dim keyExist As String = CheckForKeyPatientGlobal(newPatient.MedicalRecordNum, sConnString , dir)

	WriteToSpecifiedLog(keyExist.ToString() & vbCrLf, dir)


	


	If Not Convert.ToInt32(keyExist) > 0 Then
	 	Try

			'Dim sConnString As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
			Dim sqlConn As SqlConnection = New SqlConnection(sConnString)
			Dim sQueryString As String
			

			Try


				With newPatient


					sQueryString = "INSERT INTO [EWLC].[dbo].[Patients] " & _
					"(FirstName, " & _
					"LastName, " & _
					"TranslatorFlag, " & _
					"MedicalRecordNum, " & _
					"DateAdded, " & _
					"HIPAASigDate, " & _
					"PatientStatusDate, " & _
					"PatientStatus, " & _
					"Address1, " & _
					"Address2, " & _
					"City, " & 
					"State, " & _
					"ZipCode, " & _
					"DateOfBirth, " & _
					"Gender, " & _
					"EmailAddress, " & _
					"PhoneNum, " & _
					"PromotionsEmail, " & _
					"PromotionsText, " & _
					"PromotionsMail)" & _
					" VALUES " & _
					"('" & Replace(.FirstName, "'", "''") & _
					"', '" & Replace(.LastName, "'", "''") & _
					"', " & Replace("0", "'", "''") & _
					", '" & Replace(.MedicalRecordNum, "'", "''") & _
					"', '" & Replace("2000-07-14", "'", "''") & _
					"', '" & Replace(GetDate(), "'", "''") & _
					"', '" & Replace(GetDate(), "'", "''") & _
					"', '" & Replace("Active", "'", "''") & _
					"', '" & Replace(.Address1, "'", "''") & _
					"', '" & Replace(.Address2, "'", "''") & _
					"', '" & Replace(.City, "'", "''") & _
					"', '" & Replace(.State, "'", "''") & _
					"', " & Replace(.Zip, "'", "''") & _
					", '" & Replace(.DateOfBirth, "'", "''") & _
					"', '" & Replace(.Gender.Substring(0,1).ToString(), "'", "''") & _
					"', '" & Replace(.EmailAddress, "'", "''") & _
					"', '" & Replace(newPhone, "'", "''") & _
					"', '" & Replace(promotionsEmail, "'", "''") & _
					"', '" & Replace(promotionsText, "'", "''") & _
					"', '" & Replace(promotionsMail, "'", "''") & "')"

				End With



				WriteToSpecifiedLog(sQueryString, dir)



				sqlConn.Open()
				Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
				DBCommand.ExecuteNonQuery()

				sqlConn.Close()

				WritePatientHistoryBlankData(newPatient.MedicalRecordNum, sConnString, dir)

			Catch ex As Exception
				sqlConn.Close()
				sqlConn.Dispose()
				WriteToSpecifiedLog(ex.ToString(), dir)
				SendErrorEmail(ex.ToString())
			End Try






		Catch ex As Exception
			WriteToSpecifiedLog(ex.ToString(), dir)
			SendErrorEmail(ex.ToString())
		End Try
	Else
		'Update Statement

	    'Dim sConnString As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
        Dim sqlConn As SqlConnection = New SqlConnection(sConnString)

        Dim sQueryString As String
        Try


        	With newPatient
                sQueryString = "UPDATE  [EWLC].[dbo].[Patients] SET " & _
                    "TranslatorFlag = " & "0" & ", " & _
                    "Address1 = '" & Replace(.Address1, "'", "''") & "', " & _
                    "Address2 = '" & Replace(.Address2, "'", "''") & "', " & _
                    "City = '" & Replace(.City, "'", "''") & "', " & _
                    "State = '" & Replace(.State, "'", "''") & "', " & _
                    "ZipCode = " & Replace(.Zip, "'", "''") & ", " & _
                    "Gender = '" & Replace(.Gender.Substring(0,1).ToString(), "'", "''") & "', " & _
                    "EmailAddress = '" & Replace(.EmailAddress, "'", "''") & "', " & _
                    "PhoneNum = '" & Replace(newPhone, "'", "''") & "', " & _
                    "PromotionsEmail = '" & Replace(promotionsEmail, "'", "''") & "', " & _
                    "PromotionsText = '" & Replace(promotionsText, "'", "''") & "', " & _
                    "PromotionsMail = '" & Replace(promotionsMail, "'", "''") & "' " & _
                    "WHERE MedicalRecordNum = '" & Replace(.MedicalRecordNum, "'", "''") & "'"

          	End With


			WriteToSpecifiedLog(sQueryString & vbCrLf, dir)

            sqlConn.Open()
            Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
            DBCommand.ExecuteNonQuery()
            sqlConn.Close()

          	

        Catch ex As Exception

            sqlConn.Close()
            sqlConn.Dispose()
            WriteToSpecifiedLog(ex.ToString(), dir)
			SendErrorEmail(ex.ToString())

        End Try

	End If



	Return retString

End Function




Public Function WritePatientDataEVisitYes(ByVal newPatient As Patient, ByVal newPatientHistory As PatientHistory, strCurMeds As String, sConnString As String, dir As String) As String


	WriteToSpecifiedLog("Patient: Yes", dir)


	Dim promotionsEmail As String = "0"
	Dim promotionsText As String = "0"
	Dim promotionsMail As String = "0"
	Dim promotionsNoThankYou As String = "0"

	Dim newPhone As String = newPatient.Phone.Replace("(","")
	newPhone = newPhone.Replace(")","")
	newPhone = newPhone.Replace(" ","-")
	newPhone = newPhone.Replace(vbCrLf,"")

	With newPatient

		If  .Promos.Contains("Email") Then
			promotionsEmail = "1"
		End If
		If  .Promos.Contains("Text") Then
			promotionsText = "1"
		End If
		If  .Promos.Contains("Mail") Then
			promotionsMail = "1"
		End If

	End With

	Dim retString As String

	Dim keyExist As String = CheckForKeyPatientGlobal(newPatient.MedicalRecordNum, sConnString , dir)

	WriteToSpecifiedLog(keyExist.ToString() & vbCrLf, dir)


	


	If Not Convert.ToInt32(keyExist) > 0 Then
	 	Try

			'Dim sConnString As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
			Dim sqlConn As SqlConnection = New SqlConnection(sConnString)
			Dim sQueryString As String
			

			Try


				With newPatient


					sQueryString = "INSERT INTO [EWLC].[dbo].[Patients] " & _
					"(FirstName, " & _
					"LastName, " & _
					"TranslatorFlag, " & _
					"MedicalRecordNum, " & _
					"DateAdded, " & _
					"HIPAASigDate, " & _
					"PatientStatusDate, " & _
					"PatientStatus, " & _
					"Address1, " & _
					"Address2, " & _
					"City, " & 
					"State, " & _
					"ZipCode, " & _
					"DateOfBirth, " & _
					"Gender, " & _
					"EmailAddress, " & _
					"PhoneNum, " & _
					"PromotionsEmail, " & _
					"PromotionsText, " & _
					"PromotionsMail)" & _
					" VALUES " & _
					"('" & Replace(.FirstName, "'", "''") & _
					"', '" & Replace(.LastName, "'", "''") & _
					"', " & Replace("0", "'", "''") & _
					", '" & Replace(.MedicalRecordNum, "'", "''") & _
					"', '" & Replace(GetDate(), "'", "''") & _
					"', '" & Replace(GetDate(), "'", "''") & _
					"', '" & Replace(GetDate(), "'", "''") & _
					"', '" & Replace("Active", "'", "''") & _
					"', '" & Replace(.Address1, "'", "''") & _
					"', '" & Replace(.Address2, "'", "''") & _
					"', '" & Replace(.City, "'", "''") & _
					"', '" & Replace(.State, "'", "''") & _
					"', " & Replace(.Zip, "'", "''") & _
					", '" & Replace(.DateOfBirth, "'", "''") & _
					"', '" & Replace(.Gender.Substring(0,1).ToString(), "'", "''") & _
					"', '" & Replace(.EmailAddress, "'", "''") & _
					"', '" & Replace(newPhone, "'", "''") & _
					"', '" & Replace(promotionsEmail, "'", "''") & _
					"', '" & Replace(promotionsText, "'", "''") & _
					"', '" & Replace(promotionsMail, "'", "''") & "')"

				End With



				WriteToSpecifiedLog(sQueryString, dir)



				sqlConn.Open()
				Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
				DBCommand.ExecuteNonQuery()

				sqlConn.Close()


				WritePatientHistoryDataGlobal(newPatientHistory, newPatient.MedicalRecordNum, sConnString, dir)
		    	If Not CheckForNull(strCurMeds) = "" Then


		    		Try

			    		Dim curMedsArray As String() = strCurMeds.Split(new String() {Environment.NewLine}, StringSplitOptions.None)

					    For Each med In curMedsArray
					    	If Not Trim(med) = "" Then
					    		WriteToSpecifiedLog(med, dir)

								If med.Length() > 99 Then
									Dim medContent As String = newPatient.FirstName & " " & newPatient.LastName & vbCrLf & newPatient.MedicalRecordNum & vbCrLf & "Medications To Examine: " & med
									'SendMedicationReviewEmail("Medication Length too long:", medContent)
									MailLibrary.Send(medContent, "New Registration Form : Medication Length too long", "austinrsellers@gmail.com", "steve.sellers@techknowvation.com,ehstyler@exphs.com")
									med = med.Substring(0,99)
								End If
								WriteMedicationListUpdateGlobal(newPatient.MedicalRecordNum, med, sConnString, dir)
								If med.Contains(",") Then
									'SendErrorEmail("Medications To Examine: " & med)
									Dim medContent As String = newPatient.FirstName & " " & newPatient.LastName & vbCrLf & newPatient.MedicalRecordNum & vbCrLf & "Medications To Examine: " & med
									'SendMedicationReviewEmail("Medication Review", medContent)
									MailLibrary.Send(medContent, "New Registration Form : Medication Review", "austinrsellers@gmail.com", "steve.sellers@techknowvation.com,ehstyler@exphs.com")
									WriteToLog("Medications To Examine: " & med)
								End If
							End If
					    Next

		    		Catch ex As Exception
		    			WriteToSpecifiedLog(ex.ToString(), dir)
		    		End Try

	    			
	    		Else

					WriteToSpecifiedLog("Medications: Null", dir)

	    		End If
				

			Catch ex As Exception
				sqlConn.Close()
				sqlConn.Dispose()
				WriteToSpecifiedLog(ex.ToString(), dir)
				SendErrorEmail(ex.ToString())
			End Try


		Catch ex As Exception
			WriteToSpecifiedLog(ex.ToString(), dir)
			SendErrorEmail(ex.ToString())
		End Try
	Else
		'Update Statement

	    'Dim sConnString As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
        Dim sqlConn As SqlConnection = New SqlConnection(sConnString)

        Dim sQueryString As String
        Try


        	With newPatient
                sQueryString = "UPDATE  [EWLC].[dbo].[Patients] SET " & _
                    "HIPAASigDate = '" & Replace(GetDate(), "'", "''") & "', " & _
                    "TranslatorFlag = " & "0" & ", " & _
                    "PatientStatus = '" & "Active" & "', " & _
                    "PatientStatusDate = '" & Replace(GetDate(), "'", "''") & "', " & _
                    "Address1 = '" & Replace(.Address1, "'", "''") & "', " & _
                    "Address2 = '" & Replace(.Address2, "'", "''") & "', " & _
                    "City = '" & Replace(.City, "'", "''") & "', " & _
                    "State = '" & Replace(.State, "'", "''") & "', " & _
                    "ZipCode = " & Replace(.Zip, "'", "''") & ", " & _
                    "Gender = '" & Replace(.Gender.Substring(0,1).ToString(), "'", "''") & "', " & _
                    "EmailAddress = '" & Replace(.EmailAddress, "'", "''") & "', " & _
                    "PhoneNum = '" & Replace(newPhone, "'", "''") & "', " & _
                    "PromotionsEmail = '" & Replace(promotionsEmail, "'", "''") & "', " & _
                    "PromotionsText = '" & Replace(promotionsText, "'", "''") & "', " & _
                    "PromotionsMail = '" & Replace(promotionsMail, "'", "''") & "' " & _
                    "WHERE MedicalRecordNum = '" & Replace(.MedicalRecordNum, "'", "''") & "'"

          	End With


			WriteToSpecifiedLog(sQueryString & vbCrLf, dir)

            sqlConn.Open()
            Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
            DBCommand.ExecuteNonQuery()
            sqlConn.Close()



			WritePatientHistoryDataGlobal(newPatientHistory, newPatient.MedicalRecordNum, sConnString, dir)
	    	If Not CheckForNull(strCurMeds) = "" Then


	    		Try

		    		Dim curMedsArray As String() = strCurMeds.Split(new String() {Environment.NewLine}, StringSplitOptions.None)

				    For Each med In curMedsArray
				    	If Not Trim(med) = "" Then
				    		WriteToSpecifiedLog(med, dir)

							If med.Length() > 99 Then
								Dim medContent As String = newPatient.FirstName & " " & newPatient.LastName & vbCrLf & newPatient.MedicalRecordNum & vbCrLf & "Medications To Examine: " & med
								'SendMedicationReviewEmail("Medication Length too long:", medContent)
								MailLibrary.Send(medContent, "New Registration Form : Medication Length too long", "austinrsellers@gmail.com", "steve.sellers@techknowvation.com,ehstyler@exphs.com")
								med = med.Substring(0,99)
							End If
							WriteMedicationListUpdateGlobal(newPatient.MedicalRecordNum, med, sConnString, dir)
							If med.Contains(",") Then
								'SendErrorEmail("Medications To Examine: " & med)
								Dim medContent As String = newPatient.FirstName & " " & newPatient.LastName & vbCrLf & newPatient.MedicalRecordNum & vbCrLf & "Medications To Examine: " & med
								'SendMedicationReviewEmail("Medication Review", medContent)
								MailLibrary.Send(medContent, "New Registration Form : Medication Review", "austinrsellers@gmail.com", "steve.sellers@techknowvation.com,ehstyler@exphs.com")
								WriteToLog("Medications To Examine: " & med)
							End If
						End If
				    Next

	    		Catch ex As Exception
	    			WriteToSpecifiedLog(ex.ToString(), dir)
	    		End Try


			Else
				WriteToSpecifiedLog("Medications: Null", dir)
    		End If
          	

        Catch ex As Exception

            sqlConn.Close()
            sqlConn.Dispose()
            WriteToSpecifiedLog(ex.ToString(), dir)
			SendErrorEmail(ex.ToString())

        End Try

	End If



	Return retString

End Function









Public Function CheckForKeyPatientGlobal(MedicalRecordNum As String, sConnString As String, dir As String) As String


	Dim retBoolean As String
	Try
		'Dim sConnString As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
		Dim sqlConn As SqlConnection = New SqlConnection(sConnString)
		Dim sQueryString As String = "SELECT COUNT(*) AS retBoolean FROM [EWLC].[dbo].[Patients] where MedicalRecordNum = '" & MedicalRecordNum & "' "
		Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
		sqlConn.Open()

		Try

			Dim sqlReader As SqlDataReader = DBCommand.ExecuteReader(CommandBehavior.CloseConnection)
			Dim i As Integer
			While sqlReader.Read()
				
				
				retBoolean = Convert.ToString(sqlReader("retBoolean"))


			End While

			sqlReader.Close()

		Catch ex As Exception

			sqlConn.Close()
			WriteToSpecifiedLog(vbCrLf & ex.ToString(), dir)
			SendErrorEmail(ex.ToString())
		End Try

		sqlConn.Close()

	Catch ex As Exception
		WriteToSpecifiedLog(vbCrLf & ex.ToString(), dir)
		SendErrorEmail(ex.ToString())
	End Try

	Return retBoolean

End Function



Public Function WritePatientHistoryBlankData(MedicalRecordNum As String, sConnString As String, dir As String)
	
	Dim keyExist As String = CheckForKeyPatientHistoryGlobal(MedicalRecordNum, sConnString, dir)

	Dim retString As String


	WriteToSpecifiedLog(keyExist.ToString() & vbCrLf, dir)


	


	If Not Convert.ToInt32(keyExist) > 0 Then
	 	Try

			'Dim sConnString As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
			Dim sqlConn As SqlConnection = New SqlConnection(sConnString)
			Dim sQueryString As String
			

			Try



					sQueryString = "INSERT INTO [EWLC].[dbo].[PatientHistory] " & _
					"(MedicalRecordNum)" & _
					" VALUES " & _
					"('" & MedicalRecordNum & "')" 
					

				

				WriteToSpecifiedLog(sQueryString, dir)
				

				sqlConn.Open()
				Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
				DBCommand.ExecuteNonQuery()

				sqlConn.Close()

				

			Catch ex As Exception
				sqlConn.Close()
				sqlConn.Dispose()
				WriteToSpecifiedLog(ex.ToString(), dir)
				SendErrorEmail(ex.ToString())
			End Try


		Catch ex As Exception
			WriteToSpecifiedLog(ex.ToString(), dir)
			SendErrorEmail(ex.ToString())
		End Try
	

	End If




End Function






Public Function CheckForKeyPatientHistoryGlobal(MedicalRecordNum As String, sConnString As String, dir As String) As String


	Dim retBoolean As String
	Try
		'Dim sConnString As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
		Dim sqlConn As SqlConnection = New SqlConnection(sConnString)
		Dim sQueryString As String = "SELECT COUNT(*) AS retBoolean FROM [EWLC].[dbo].[PatientHistory] where MedicalRecordNum = '" & MedicalRecordNum & "' "
		Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
		sqlConn.Open()

		Try

			Dim sqlReader As SqlDataReader = DBCommand.ExecuteReader(CommandBehavior.CloseConnection)
			Dim i As Integer
			While sqlReader.Read()
				
				
				retBoolean = Convert.ToString(sqlReader("retBoolean"))


			End While

			sqlReader.Close()

		Catch ex As Exception

			sqlConn.Close()
			WriteToSpecifiedLog(vbCrLf & ex.ToString(), dir)
			SendErrorEmail(ex.ToString())
		End Try

		sqlConn.Close()

	Catch ex As Exception
		WriteToSpecifiedLog(vbCrLf & ex.ToString(), dir)
		SendErrorEmail(ex.ToString())
	End Try

	Return retBoolean

End Function


Public Function WritePatientHistoryDataGlobal(newPatientHistory As PatientHistory, MedicalRecordNum As String, sConnString As String, dir As String)




	
	Dim keyExist As String = CheckForKeyPatientHistoryGlobal(MedicalRecordNum, sConnString, dir)




	Dim heartAttackDate As String = ConvertDate(newPatientHistory.heartAttack)
	Dim strokeDate As String = ConvertDate(newPatientHistory.strokeDate)



	Dim highBloodPressure As String = "0"
	Dim heartAttack As String = "0"
	Dim diabetesMellitus As String = "0"
	Dim hypothyroid As String = "0"
	Dim hyperthyroid As String = "0"
	Dim reflux As String = "0"
	Dim glaucoma As String = "0"
	Dim coronaryArteryDisease As String = "0"
	Dim stroke As String = "0"
	Dim highCholesterolTriglycerides As String = "0"
	Dim congestiveHeartFailure As String = "0"
	Dim addAdhd As String = "0"
	Dim otherMedHist As String = "0"
	'-----'
	Dim hysterectomy As String = "0"
	Dim tubalLigation As String = "0"
	Dim gallbladderRemoval As String = "0"
	Dim appendectomy As String = "0"
	Dim coronaryStents As String = "0"
	Dim coronaryArteryBypassGraft As String = "0"
	Dim carotidEndarterectomy As String = "0"
	Dim cSection As String = "0"
	Dim otherSurgHist As String = "0"
	'-------'
	Dim cigarettes As String = "0"
	Dim smokelessTobacco As String = "0"
	Dim alcoholConsumption As String = "0"
	Dim illicitDrugs As String = "0"
	'-------'
	Dim swelling As String = "0"
	Dim fatigue As String = "0"
	Dim skin As String = "0"
	Dim excessWeight As String = "0"
	'-------'
	Dim largePortions As String = "0"
	Dim sweetsSnacks As String = "0"
	Dim carbonatedDrinks As String = "0"
	'----'
	Dim physicalActivity As String = "0"
	'----'
	Dim alcoholFrequency As String = "0"




	With newPatientHistory

		If .curMedHistory.Contains("High Blood Pressure") Then
			highBloodPressure = "1"
		End If
		If .curMedHistory.Contains("Heart Attack") Then
			heartAttack = "1"
		End If
		If .curMedHistory.Contains("Diabetes Mellitus") Then
			diabetesMellitus = "1"
		End If
		If .curMedHistory.Contains("Hypothyroid") Then
			hypothyroid = "1"
		End If
		If .curMedHistory.Contains("Hyperthyroid") Then
			hyperthyroid = "1"
		End If
		If .curMedHistory.Contains("Reflux") Then
			reflux = "1"
		End If
		If .curMedHistory.Contains("Glaucoma") Then
			glaucoma = "1"
		End If
		If .curMedHistory.Contains("Coronary Artery Disease") Then
			coronaryArteryDisease = "1"
		End If
		If .curMedHistory.Contains("Stroke") Then
			stroke = "1"
		End If
		If .curMedHistory.Contains("High Cholesterol/Triglycerides") Then
			highCholesterolTriglycerides = "1"
		End If
		If .curMedHistory.Contains("Congestive Heart Failure") Then
			congestiveHeartFailure = "1"
		End If
		If .curMedHistory.Contains("ADD/ADHD") Then
			addAdhd = "1"
		End If
		If .curMedHistory.Contains("Other (Please explain):") Then
			otherMedHist = "1"
		End If
		'-------------'
		If .surgHistory.Contains("Hysterectomy") Then
			hysterectomy = "1"
		End If
		If .surgHistory.Contains("Tubal Ligation") Then
			tubalLigation = "1"
		End If
		If .surgHistory.Contains("Gallbladder Removal") Then
			gallbladderRemoval = "1"
		End If
		If .surgHistory.Contains("Appendectomy") Then
			appendectomy = "1"
		End If
		If .surgHistory.Contains("Coronary Stents") Then
			coronaryStents = "1"
		End If
		If .surgHistory.Contains("Coronary Artery Bypass Graft") Then
			coronaryArteryBypassGraft = "1"
		End If
		If .surgHistory.Contains("Carotid Endarterectomy") Then
			carotidEndarterectomy = "1"
		End If
		If .surgHistory.Contains("C-Section") Then
			cSection = "1"
		End If
		If .surgHistory.Contains("Other") Then
			otherSurgHist = "1"
		End If
		'----------'
		If .socialHistory.Contains("Cigarettes") Then
			cigarettes = "1"
		End If
		If .socialHistory.Contains("Smokeless Tobacco") Then
			smokelessTobacco = "1"
		End If
		If .socialHistory.Contains("Alcohol Consumption") Then
			alcoholConsumption = "1"
		End If
		If .socialHistory.Contains("Illicit Drugs:") Then
			illicitDrugs = "1"
		End If
		'-----'
		If .revSymptoms.Contains("Swelling") Then
			swelling = "1"
		End If
		If .revSymptoms.Contains("Fatigue") Then
			fatigue = "1"
		End If
		If .revSymptoms.Contains("Skin") Then
			skin = "1"
		End If
		If .revSymptoms.Contains("Excess Weight") Then
			excessWeight = "1"
		End If
		'-----'
		If .eatHabits.Contains("Large Portions") Then
			largePortions = "1"
		End If
		If .eatHabits.Contains("Sweets/Snacks") Then
			sweetsSnacks = "1"
		End If
		If .eatHabits.Contains("Carbonated Drinks") Then
			carbonatedDrinks = "1"
		End If
		'-------'
		If .physActivity.Contains("Walk or Other") Then
			physicalActivity = "1"
		End If
		'----'

		If .alcoholConsumption.Contains("Infrequent Social Drinking") Then
			alcoholFrequency = "1"
		End If
		If .alcoholConsumption.Contains("Frequent Small Amounts") Then
			alcoholFrequency = "2"
		End If
		If .alcoholConsumption.Contains("Frequent Large Amounts") Then
			alcoholFrequency = "3"
		End If














	End With

	Dim retString As String


	WriteToSpecifiedLog(keyExist.ToString() & vbCrLf, dir)


	


	If Not Convert.ToInt32(keyExist) > 0 Then
	 	Try

			'Dim sConnString As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
			Dim sqlConn As SqlConnection = New SqlConnection(sConnString)
			Dim sQueryString As String
			

			Try


				With newPatientHistory

					sQueryString = "INSERT INTO [EWLC].[dbo].[PatientHistory] " & _
					"(HeartAttack, " & _
					"HeartAttackDate, " & _
					"StrokeDate, " & _
					"CarotidEndarterectomy, " & _
					"MedicalRecordNum, " & _
					"Hypertension, " & _
					"DiabetesMellitus, " & _
					"Hypothyroid, " & _
					"Reflux, " & _
					"Glaucoma, " & _
					"CoronaryArteryDisease, " & _
					"Stroke, " & _
					"HighCholesterolTriglycerides, " & _
					"CongestiveHeartFailure, " & _
					"Hyperthyroid, " & 
					"MedHistOther, " & _
					"MedHistOtherText, " & _
					"OtherSurgHistText, " & _
					"Hysterectomy, " & _
					"GallbladderRemoval, " & _
					"CoronaryStents, " & _
					"CSection, " & _
					"TubalLigation, " & _
					"Appendectomy, " & _
					"CoronaryArteryBypassGraft, " & _
					"OtherSurgHist, " & _
					"Cigarettes, " & _
					"SmokelessTobacco, " & _
					"AlcoholConsumptionSocialHistory, " & _
					"IllicitDrugs, " & _
					"IllicitDrugsText, " & _
					"UnwantedExcessWeight, " & _
					"Fatigue, " & _
					"Swelling, " & _
					"SkinHairNailProblems, " & _
					"LargePortions, " & _
					"SweetsSnacks, " & _
					"CarbonatedDrinks, " & _
					"DailyExercise, " & _
					"MedAllergiesText, " & _
					"ADDorADHD, " & _
					"AlcoholConsumptionFrequency)" & _
					" VALUES " & _
					"('" & Replace(heartAttack, "'", "''") & _
					"', '" & Replace(heartAttackDate, "'", "''") & _
					"', '" & Replace(strokeDate, "'", "''") & _
					"', '" & Replace(carotidEndarterectomy, "'", "''") & _
					"', '" & Replace(MedicalRecordNum, "'", "''") & _
					"', '" & Replace(highBloodPressure, "'", "''") & _
					"', '" & Replace(diabetesMellitus, "'", "''") & _
					"', '" & Replace(hypothyroid, "'", "''") & _
					"', '" & Replace(reflux, "'", "''") & _
					"', '" & Replace(glaucoma, "'", "''") & _
					"', '" & Replace(CoronaryArteryDisease, "'", "''") & _
					"', '" & Replace(stroke, "'", "''") & _
					"', '" & Replace(highCholesterolTriglycerides, "'", "''") & _
					"', '" & Replace(congestiveHeartFailure, "'", "''") & _
					"', '" & Replace(hyperthyroid, "'", "''") & _
					"', '" & Replace(otherMedHist, "'", "''") & _
					"', '" & Replace(.medOther, "'", "''") & _
					"', '" & Replace(.other, "'", "''") & _
					"', '" & Replace(hysterectomy, "'", "''") & _
					"', '" & Replace(gallbladderRemoval, "'", "''") & _
					"', '" & Replace(coronaryStents, "'", "''") & _
					"', '" & Replace(cSection, "'", "''") & _
					"', '" & Replace(tubalLigation, "'", "''") & _
					"', '" & Replace(appendectomy, "'", "''") & _
					"', '" & Replace(CoronaryArteryBypassGraft, "'", "''") & _
					"', '" & Replace(otherSurgHist, "'", "''") & _
					"', '" & Replace(cigarettes, "'", "''") & _
					"', '" & Replace(smokelessTobacco, "'", "''") & _
					"', '" & Replace(alcoholConsumption, "'", "''") & _
					"', '" & Replace(illicitDrugs, "'", "''") & _
					"', '" & Replace(.illicitDrugs, "'", "''") & _
					"', '" & Replace(excessWeight, "'", "''") & _
					"', '" & Replace(fatigue, "'", "''") & _
					"', '" & Replace(swelling, "'", "''") & _
					"', '" & Replace(skin, "'", "''") & _
					"', '" & Replace(largePortions, "'", "''") & _
					"', '" & Replace(sweetsSnacks, "'", "''") & _
					"', '" & Replace(carbonatedDrinks, "'", "''") & _
					"', '" & Replace(physicalActivity, "'", "''") & _
					"', '" & Replace(.allergiesToMeds, "'", "''") & _
					"', '" & Replace(addAdhd, "'", "''") & _
					"', '" & Replace(alcoholFrequency, "'", "''") & "')"
					

				End With
				

				WriteToSpecifiedLog(sQueryString, dir)
				

				sqlConn.Open()
				Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
				DBCommand.ExecuteNonQuery()

				sqlConn.Close()

				

			Catch ex As Exception
				sqlConn.Close()
				sqlConn.Dispose()
				WriteToSpecifiedLog(ex.ToString(), dir)
				SendErrorEmail(ex.ToString())
			End Try


		Catch ex As Exception
			'WriteToLog(ex.ToString())
			WriteToSpecifiedLog(ex.ToString(), dir)
		End Try
	Else
		'Update Statement

	    'Dim sConnString As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
        Dim sqlConn As SqlConnection = New SqlConnection(sConnString)

        Dim sQueryString As String
        Try


        	With newPatientHistory




                sQueryString = "UPDATE  [EWLC].[dbo].[PatientHistory] SET " & _
                    "HeartAttack = '" & Replace(heartAttack, "'", "''") & "', " & _
                    "HeartAttackDate = '" & Replace(heartAttackDate, "'", "''") & "', " & _
                    "StrokeDate = '" & Replace(strokeDate, "'", "''") & "', " & _
                    "CarotidEndarterectomy = '" & Replace(carotidEndarterectomy, "'", "''") & "', " & _
                    "Hypertension = '" & Replace(highBloodPressure, "'", "''") & "', " & _
                    "DiabetesMellitus = '" & Replace(diabetesMellitus, "'", "''") & "', " & _
                    "Hypothyroid = '" & Replace(hypothyroid, "'", "''") & "', " & _
                    "Reflux = '" & Replace(reflux, "'", "''") & "', " & _
                    "Glaucoma = '" & Replace(glaucoma, "'", "''") & "', " & _
                    "CoronaryArteryDisease = " & Replace(coronaryArteryDisease, "'", "''") & ", " & _
                    "Stroke = '" & Replace(stroke, "'", "''") & "', " & _
                    "HighCholesterolTriglycerides = '" & Replace(highCholesterolTriglycerides, "'", "''") & "', " & _
                    "CongestiveHeartFailure = '" & Replace(congestiveHeartFailure, "'", "''") & "', " & _
                    "Hyperthyroid = '" & Replace(hyperthyroid, "'", "''") & "', " & _
                    "MedHistOther = '" & Replace(otherMedHist, "'", "''") & "', " & _
                    "MedHistOtherText = '" & Replace(.medOther, "'", "''") & "', " & _
                    "OtherSurgHistText = '" & Replace(.other, "'", "''") & "', " & _
                    "Hysterectomy = '" & Replace(hysterectomy, "'", "''") & "', " & _
                    "GallbladderRemoval = '" & Replace(gallbladderRemoval, "'", "''") & "', " & _
                    "CoronaryStents = '" & Replace(coronaryStents, "'", "''") & "', " & _
                    "CSection = '" & Replace(cSection, "'", "''") & "', " & _
                    "TubalLigation = '" & Replace(tubalLigation, "'", "''") & "', " & _
                    "Appendectomy = '" & Replace(appendectomy, "'", "''") & "', " & _
                    "CoronaryArteryBypassGraft = '" & Replace(CoronaryArteryBypassGraft, "'", "''") & "', " & _
                    "OtherSurgHist = '" & Replace(otherSurgHist, "'", "''") & "', " & _
                    "Cigarettes = '" & Replace(cigarettes, "'", "''") & "', " & _
                    "SmokelessTobacco = '" & Replace(smokelessTobacco, "'", "''") & "', " & _
                    "AlcoholConsumptionSocialHistory = '" & Replace(alcoholConsumption, "'", "''") & "', " & _
                    "IllicitDrugs = '" & Replace(illicitDrugs, "'", "''") & "', " & _
                    "IllicitDrugsText = '" & Replace(.illicitDrugs, "'", "''") & "', " & _
                    "UnwantedExcessWeight = '" & Replace(excessWeight, "'", "''") & "', " & _
                    "Fatigue = '" & Replace(fatigue, "'", "''") & "', " & _
                    "Swelling = '" & Replace(swelling, "'", "''") & "', " & _
                    "SkinHairNailProblems = '" & Replace(skin, "'", "''") & "', " & _
                    "LargePortions = '" & Replace(largePortions, "'", "''") & "', " & _
                    "SweetsSnacks = '" & Replace(sweetsSnacks, "'", "''") & "', " & _
                    "CarbonatedDrinks = '" & Replace(carbonatedDrinks, "'", "''") & "', " & _
                    "DailyExercise = '" & Replace(physicalActivity, "'", "''") & "', " & _
                    "MedAllergiesText = '" & Replace(.allergiesToMeds, "'", "''") & "', " & _
                    "ADDorADHD = '" & Replace(addAdhd, "'", "''") & "', " & _
                    "AlcoholConsumptionFrequency = '" & Replace(alcoholFrequency, "'", "''") & "' " & _
                    "WHERE MedicalRecordNum = '" & Replace(MedicalRecordNum, "'", "''") & "'"



          	End With

			WriteToSpecifiedLog(sQueryString & vbCrLf, dir)

            sqlConn.Open()
            Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
            DBCommand.ExecuteNonQuery()
            sqlConn.Close()

          	

        Catch ex As Exception

            sqlConn.Close()
            sqlConn.Dispose()
            WriteToSpecifiedLog(ex.ToString(), dir)
			SendErrorEmail(ex.ToString())

        End Try

	End If




End Function


Public Function WriteMedicationListUpdateGlobal(MedicalRecordNum As String, curMeds As String, sConnString As String, dir As String)

		

		Dim retBoolean As String = CheckForMedicationGlobal(MedicalRecordNum, curMeds, sConnString, dir)
		WriteToSpecifiedLog(retBoolean, dir)

		If Not Convert.ToInt32(retBoolean) > 0 Then



		 	Try

				'Dim sConnString As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
				Dim sqlConn As SqlConnection = New SqlConnection(sConnString)
				Dim sQueryString As String
				

				Try


					

						sQueryString = "INSERT INTO [EWLC].[dbo].[PatientMedicationListUpdates] " & _
						"(MedicalRecordNum, " & _
						"MedicationListDate, " & _
						"Medication, " & _
						"RecordType)" & _
						" VALUES " & _
						"('" & Replace(MedicalRecordNum, "'", "''") & _
						"', '" & Replace(GetDate(), "'", "''") & _
						"', '" & Replace(curMeds, "'", "''") & _
						"', '" & Replace("Medication", "'", "''") & "')"
						

				
					

					WriteToSpecifiedLog(sQueryString, dir)
					

					sqlConn.Open()
					Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
					DBCommand.ExecuteNonQuery()

					sqlConn.Close()

					

				Catch ex As Exception
					sqlConn.Close()
					sqlConn.Dispose()
					WriteToSpecifiedLog(ex.ToString(), dir)
					SendErrorEmail(ex.ToString())
				End Try


			Catch ex As Exception
				WriteToSpecifiedLog(ex.ToString(), dir)
				SendErrorEmail(ex.ToString())
			End Try


		End If


End Function



Public Function CheckForMedicationGlobal(MedicalRecordNum As String, Medication As String, sConnString As String, dir As String) As String


	Dim retBoolean As String = "0"
	Try
		'Dim sConnString As String = ConfigurationManager.ConnectionStrings("Conn2").ConnectionString
		Dim sqlConn As SqlConnection = New SqlConnection(sConnString)
		Dim sQueryString As String = "SELECT COUNT(*) AS retBoolean FROM [EWLC].[dbo].[PatientMedicationListUpdates] where MedicalRecordNum = '" & MedicalRecordNum & "' AND Medication = '" & Replace(Medication, "'", "''") & "'"
		Dim DBCommand As New SqlCommand(sQueryString, sqlConn)
		sqlConn.Open()

		Try

			Dim sqlReader As SqlDataReader = DBCommand.ExecuteReader(CommandBehavior.CloseConnection)
			Dim i As Integer
			While sqlReader.Read()
				
				
				retBoolean = Convert.ToString(sqlReader("retBoolean"))


			End While

			sqlReader.Close()

		Catch ex As Exception

			sqlConn.Close()
			WriteToLog(vbCrLf & ex.ToString())
			SendErrorEmail(ex.ToString())
		End Try

		sqlConn.Close()

	Catch ex As Exception
		WriteToLog(vbCrLf & ex.ToString())
		SendErrorEmail(ex.ToString())
	End Try

	Return retBoolean

End Function



    
    
 
    
End Class







'Generate Patient Object -------

Public Class Patient

	Public MedicalRecordNum As String
	Public FirstName As String
	Public LastName As String
 	Public Address1 As String
 	Public Address2 As String
 	Public City As String
 	Public State As String
 	Public Zip As String
 	Public DateOfBirth As String
 	Public Gender As String
 	Public EmailAddress As String
 	Public Phone As String
 	Public Promos As String

End Class

Public Class PatientHistory


	Public medHistory As String
	Public curMedHistory As String
	Public heartAttack As String
	Public strokeDate As String
	Public other As String
	Public medOther As String
	Public surgHistory As String
	Public socialHistory As String
	Public alcoholConsumption As String
	Public illicitDrugs As String
	Public revSymptoms As String
	Public eatHabits As String
	Public physActivity As String
	Public allergiesToMeds As String
	Public curMeds As String
	Public Waiver As String
	Public Notice As String
	Public Acknowledgement As String
	Public Signature As String
	Public printedFirstName As String
	Public printedLastName As String
	Public todaysDate As String
	Public above18 As String
	Public Consent As String
	Public gSig As String
	Public pDob As String
	Public gPrintedName As String
	Public Thanks As String



End Class



Public Class EVisitDetails


	Public todaysDate As String
	Public above18 As String
	Public Consent As String
	Public gSig As String
	Public pDob As String
	Public gPrintedName As String
	Public haveID As String
	Public formOfPayment As String
	Public haveScale As String
	Public autoBloodPressure As String
	Public measuringTape As String
	Public haveWebcam  As String
	Public whichDevice As String


End Class








